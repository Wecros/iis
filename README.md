# IIS - Fituska-v2

IIS BUT FIT Winter Course 2021 of 5th semester. Information System implementation for student's forum.

## Installation

1. Install [NVM](https://github.com/creationix/nvm) (if you haven't already)
    
        wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash

2. Clone repository

        cd ~/pathToRepository
        git clone https://gitlab.com/Wecros/iis.git

3. Set up frontend environment

        cd ~/pathToRepository/iis/src/frontend/fituska
        nvm install 14.16.0
        nvm use 14.16.0
        npm install
    If you run into problems with missing `nvm`, try restarting the terminal

4. Set up backend environment

        pip install -r requirements.txt

5. Copy enviroment template to actual environment config file and (optionally) change the data

        cp .env-template .env

    - I recommend setting your own SECRET_KEY via:
        
            openssl rand -hex 32

## Development

- Start backend and frontend simultaneously

        npm start
        python src/backend/main.py

### Database

- We use `docker` to run database locally. You can substitute the postgres password, user and db name however you want.

        docker run --name fituska -dp 5432:5432 -e POSTGRES_PASSWORD=fituska -e POSTGRES_USER=fituska-user -e POSTGRES_DB=fituska-db postgres:11

    - To stop the container, run this command with the container's name:

        docker stop fituska

    - To start non-running container, run:
        
        docker start fituska

    - Some other useful commands include:
        
        docker ps  # list running containers
        docker ps -a  # list all containers
        docker kill <container_name>  # destroy a container

- Now save the credentials used to create the database into `.env` configuration file.

- Next step is to migrate the database, you can either use `alembic upgrade head` or just 
hope sqlalchemy will magically create the right setup for you automatically.

### Frontend

To see the application go to `127.0.0.1:3000` address.

### Backend

Backend is written in [FastAPI](https://fastapi.tiangolo.com/).
To see the server go to `127.0.0.1:8000` address.
To view the API in Swagger UI go to the docs endpoint at `127.0.0.1:8000/docs`
or alternatively at `127.0.0.1:8000/redoc`.
FastAPI automatically generates OpenAPI schema, you can access it at `127.0.0.1:8000/openapi.json` if interested.
