import React, {useContext} from 'react'
import { Switch, Route, NavLink, Redirect } from 'react-router-dom'
import {Me, Courses, Fame, Login, Register, Course, Question, Admin} from './components/screens'
import PersonIcon from '@mui/icons-material/Person';
import './App.css'
import { UserContext } from './providers/UserContext'

function App() {
  const {logout, token, role} = useContext(UserContext)
  
  const handleLogout = () => {
    logout()
  }

  return (
    <div>
    <div className='App'>
      <header>
        <div className='menu'>
          <NavLink activeClassName="menuItemActive" to="/courses" style={{textDecoration: 'none'}}>
            <div className="menuItem">
              Všechny kurzy
            </div>
          </NavLink>
          <NavLink activeClassName="menuItemActive" to="/fame" style={{textDecoration: 'none'}}>
            <div className="menuItem">
              Síň slávy
            </div>
          </NavLink>
          {token ? 
            <NavLink activeClassName="menuItemActive" to="/admin" style={{textDecoration: 'none'}}>
              <div className="menuItem">
                Administrace
              </div>
            </NavLink>
            :
            ''
          }
        </div>
        <div className='menu'>
          {token ? 
            <>
              <NavLink activeClassName="menuItemActive" to="/me" style={{textDecoration: 'none'}}>
                <div className='menuItem' style={{padding:'7px'}}>
                  <PersonIcon/>
                </div>
              </NavLink>
              <div onClick={handleLogout} className="menuItem">
                  Odhlásit se
              </div>
            </>
            : 
            <>
              <NavLink activeClassName="menuItemActive" to="/login" style={{textDecoration: 'none'}}>
                <div className="menuItem">
                  Příhlásit se
                </div>
              </NavLink>
              <NavLink activeClassName="menuItemActive" to="/register" style={{textDecoration: 'none'}}>
                <div className="menuItem">
                  Registrovat se
                </div>
              </NavLink>
            </>
          }
        </div>
      </header>
    </div>
    <div className='content'>
      <Switch>
        <Route exact path="/courses">
          <Courses />
        </Route>
        <Route exact path="/fame">
          <Fame />
        </Route>
        <Route component={Login} exact path="/login" />
        <Route exact path="/register">
          <Register />
        </Route>
        <Route component={Course} exact path="/course/:courseId" />
        <Route component={Question} exact path='/question/:category_id/:question_id/:course_id'/>
        <Route component={Admin} exact path='/admin'/>
        <Route component={Me} exact path='/me'/>
        <Route exact path="/">
          <Redirect to={{pathname: '/courses'}} />
        </Route>
      </Switch>
    </div>
    </div>
  );
}

export default App;
