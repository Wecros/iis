import React, { createContext, useState } from "react";
import { useHistory } from "react-router";

export const UserContext = createContext();

export const UserProvider = ({children}) => {
  const history = useHistory()
  const [token, setToken] = useState(localStorage.getItem("fituskaToken"));
  const [role, setRole] = useState(localStorage.getItem("fituskaRole"))
  const login = (token) => {
    setToken(token)
    localStorage.setItem("fituskaToken", token);
  }
  const logout = () => {
    setToken('')
    setRole('')
    localStorage.setItem("fituskaToken", '');
    localStorage.setItem("fituskaRole", role);
    history.push('/login')
  }
  const updateRole = (role) => {
    setRole(role)
    localStorage.setItem("fituskaRole", role);
  }
  const sendRequest = async (method, api, body) => {
    const requestObject = {
      method: method,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        },
    }
    if(method === 'POST' || method === 'PUT' || method === 'DELETE')
    {
      requestObject.body = body
    }
    if(token)
    {
      requestObject.headers.Authorization = "Bearer " + token
    }
    try {
      const response = await fetch(api, requestObject);
      if(response.ok)
      {
        const data = await response.json();
        return data
      }
      else {
        if(response.status === 401)
        {
          logout()
          return
        }
        return response.status
      }
      } catch(error) {
        return error
    } 
  }

  return (
    <UserContext.Provider value={{login, logout, token, sendRequest, role, updateRole}}>
      {children}
    </UserContext.Provider>
  );
};
