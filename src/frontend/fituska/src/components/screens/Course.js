import React, {useState, useContext, useEffect} from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import TextField from '@mui/material/TextField';
import { Typography } from '@mui/material';
import {QuestionCard} from '../cards'
import TextareaAutosize from '@mui/material/TextareaAutosize';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import {AddCategory, RemoveCategory} from '../modals'
import { UserContext } from '../../providers/UserContext'
import { useParams } from "react-router-dom";
import './Course.css'

const Course = props => {
  const {token, sendRequest, role} = useContext(UserContext)
  const [questions, setQuestions] = useState([])
  const [filterCategory, setFilterCategory] = useState(-1)
  const [categories, setCategories] = useState([])

  const [isAddCategory, setAddCategory] = useState(false)
  const [isRemoveCategory, setRemoveCategory] = useState(false)

  const courseId = useParams()

  const [data, setData] = useState({})

  const [isTeacher, setTeacher] = useState(false)
  const [isStudentEnrolled, setStudentEnrolled] = useState(false)
  const [isStudentPending, setStudentPending] = useState(false)

  const fetchEnrolledStudent = () =>
  {
    const response = sendRequest('GET', 'http://localhost:8000/api/me/courses/registered/', {})
    response.then(res => {
      res.forEach(element => {
        if (element.id === parseInt(courseId.courseId))
        {
          setStudentEnrolled(true)
        }
      })
    })
  }

  const fetchPendingStudent = () =>
  {
    const response = sendRequest('GET', 'http://localhost:8000/api/me/courses/pending_registration/', {})
    response.then(res => {
      res.forEach(element => {
        if (element.id === parseInt(courseId.courseId))
        {
          setStudentPending(true)
        }
      })
    })
  }

  const fetchCourse = () =>
  {
    const response = sendRequest('GET', 'http://localhost:8000/api/courses/public/', {})
    response.then(res => {
      res.forEach(element => {
        if (element.id === parseInt(courseId.courseId))
        {
          console.log(element)
          setData(element)
          setCategories(element.categories)
          fetchQuestions(filterCategory)
          if(token)
          {
            fetchTeacher(element)
            fetchEnrolledStudent()
            fetchPendingStudent()
          }
        }
      })
    })
  }

  const fetchTeacher = (data) => {
    const response = sendRequest('GET', 'http://localhost:8000/api/me/courses/teach', {})
    response.then(res => {
      if(res !== 400)
      {
        res.forEach((course) => {
          console.log(course.id)
          console.log(data.id)
          if(course.id === data.id)
          {
            setTeacher(true)
          }
        })
      }
    })
  }

  useEffect(()=> {
    fetchCourse()
  }, [isAddCategory])


  const fetchQuestions = (categoryId) => {
    let api = ''
    if(categoryId === -1)
    {
      api = 'http://localhost:8000/api/course/' + courseId.courseId + '/questions/'
    }
    else {
      api = 'http://localhost:8000/api/categories/' + categoryId + '/questions'
    }
    const response = sendRequest('GET', api, {})
    response.then(res => setQuestions(res))
  }

  const handleFilter = (e) => {
    setFilterCategory(e.target.value)
    fetchQuestions(e.target.value)
  }


  const handleAddCategory = () => {
    setAddCategory(true)
  }

  const handleRemoveCategory = () => {
    setRemoveCategory(true)
  }

  const [questionCategory, setQuestionCategory] = useState('')
  const [questionTitle, setQuestionTitle] = useState('')
  const [questionText, setQuestionText] = useState('')

  const handleQuestionCategory = (e) => {
    setQuestionCategory(e.target.value)
  }
  const handleQuestionTitle = (e) => {
    setQuestionTitle(e.target.value)
  }
  const handleQuestionText = (e) => {
    setQuestionText(e.target.value)
  }

  const handleQuestion = (e) =>
  {
    e.preventDefault()
    const body = {
      name: questionTitle,
      details: questionText,
    }
    const data = sendRequest('POST', 'http://localhost:8000/api/categories/' + questionCategory + '/questions', JSON.stringify(body))
    data.then(res => {
      console.log(res)
      fetchQuestions(filterCategory)
      setQuestionCategory('')
      setQuestionTitle('')
      setQuestionText('')
    })
  }

  const handleRegisterToCourse = () => {
    const response = sendRequest('POST', 'http://localhost:8000/api/courses/register/' + data.id, {})
    response.then(res => {
      fetchPendingStudent()
      fetchEnrolledStudent()
    })
  }

  return (
    <div className='card'>
      {isAddCategory === false ? ''
        :
        <AddCategory isAddCategory={isAddCategory} setAddCategory={setAddCategory} id={data.id} />
      }
      {isRemoveCategory === false ? ''
        :
        <RemoveCategory isRemoveCategory={isRemoveCategory} setRemoveCategory={setRemoveCategory} id={data.id} />
      }
      <Card>
        <CardContent>
          <div className='window'>
            <div className="title">
              <Typography variant='h4'>
                  {data.name}
              </Typography>
            </div>
            <div style={{margin: '10px'}}>
              <Typography variant='caption'>
                  {data.description}
              </Typography>
            </div>
            <div className='filter'>
              <div className='filterText'>
                <Typography variant='h6'>
                  Filtrovat podle kategorie:
                </Typography>
              </div>
              <div>
                <FormControl variant="standard" sx={{ m: 1, minWidth: '120px' }}>
                  <InputLabel id="demo-simple-select-standard-label">Kategórie</InputLabel>
                  <Select
                    labelId="demo-simple-select-standard-label"
                    id="demo-simple-select-standard"
                    value={filterCategory}
                    onChange={handleFilter}
                    label="Age"
                  >
                    <MenuItem value={-1}>Všechny</MenuItem>
                    {categories.map((data, idx) => (
                      <MenuItem value={data.id} key={idx}>{data.name}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </div>
              {(token && isTeacher) || (role !== "standard") ?
                <>
                  <div>
                    <Button onClick={handleAddCategory} variant='contained' sx={{margin: '5px'}}>Přidat kategorii</Button>
                    <Button onClick={handleRemoveCategory} variant='contained' sx={{margin: '5px'}}>Odstranit kategorii</Button>
                  </div>
                </>
                :
                ''
              }
              {token && (!isStudentEnrolled && !isStudentPending && !isTeacher) ?
                <div>
                  <Button onClick={handleRegisterToCourse} variant='contained' sx={{margin: '5px'}}>Registrovat se do kurzu</Button>
                </div>
                :
                ''
              }
              {token && isStudentPending ?
                <div>
                  <Button disabled variant='contained' sx={{margin: '5px'}}>Čeká se na schválení do kurzu</Button>
                </div>
                :
                ''
              }
            </div>
            {questions.map((question, idx) => (
              <QuestionCard data={question} courseId={data.id} key={idx}/>
            ))}
            {token && (isStudentEnrolled || isTeacher)?
              <div className='newQuestion'>
                <Card>
                  <CardContent>
                    <form onSubmit={handleQuestion}>
                      <div className='questionFlex'>
                        <Typography>Zadejte otázku</Typography>
                        <TextField
                          id="standard-basic"
                          label="Název"
                          variant="standard"
                          onChange={handleQuestionTitle}
                          sx={{width: '200px', margin: '10px'}}
                          required
                          value={questionTitle}
                        />
                        <TextareaAutosize
                          aria-label="minimum height"
                          minRows={4}
                          style={{ width: '100%' }}
                          onChange={handleQuestionText}
                          required
                          value={questionText}
                        />
                      </div>
                      <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                        <InputLabel id="demo-simple-select-standard-label">Kategórie</InputLabel>
                        <Select
                          labelId="demo-simple-select-standard-label"
                          id="demo-simple-select-standard"
                          value={questionCategory}
                          onChange={handleQuestionCategory}
                          label="Age"
                          required
                        >
                          {categories.map((data) => (
                            <MenuItem value={data.id} key={data.id}>{data.name}</MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                      <Button variant="contained" sx={{margin:'5px', marginTop: '15px'}} type='submit'>Odeslat</Button>
                    </form>
                  </CardContent>
                </Card>
              </div>
              :
              ''
            }
          </div>
        </CardContent>
      </Card>
    </div>
  )
}


export default Course
