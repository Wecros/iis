import React, {useState, useContext} from 'react'
import { Card, CardContent, Typography, Button, TextField } from '@mui/material'
import { UserContext } from '../../providers/UserContext'
import { useHistory } from "react-router-dom";
import './Login.css'

const Login = () => {
  const {login, updateRole} = useContext(UserContext)
  const [username, setUserName] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
  const history = useHistory()

  const loginCall = async () => {
    try {
      let response = await fetch('http://localhost:8000/api/token', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: new URLSearchParams({
          'username': username,
          'password': password
        }).toString()
      })
      const data = await response.json()
      if(response.status === 200)
      {
        login(data.access_token)
        const requestObject = {
          method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              Authorization: "Bearer " + data.access_token,
            },
        }
        response = await fetch('http://localhost:8000/api/me', requestObject);
        if(response.ok)
        {
          const me = await response.json();
          console.log(me.role)
          updateRole(me.role)
          history.push({
            pathname: '/courses'
          })
        }
        else if(response.status === 401)
        {
          setError('Nesprávné jméno nebo heslo')
        }
        else {
          const me = await response.json();
          updateRole(me.role)
          history.push({
            pathname: '/courses'
          })
        }
      }
      else if(response.status === 410)
      {
        setError('Váš účet byl deaktivován')
      }
      else {
        setError('Nesprávné jméno nebo heslo')
      }
    }
    catch(error) {
      console.log(error)
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    loginCall()
  }

  const handleUserChange = (e) => {
    setUserName(e.target.value)
  }

  const handlePassChange = (e) => {
    setPassword(e.target.value)
  }


  return (
    <Card sx={{margin: '10px'}}>
      <CardContent>
        <Typography sx={{color:'red'}}>{error}</Typography>
        <form onSubmit={handleSubmit}>
          <div>
            <TextField variant="outlined" fullWidth type="text" label="Uživatelské jméno" margin="normal"
            name="username" value={username} onChange={handleUserChange} required/>
          </div>
          <div>
            <TextField variant="outlined" fullWidth type="password" label="Heslo" margin="normal"
            name="password" value={password} onChange={handlePassChange} required/>
          </div>
          <div >
            <Button type='submit' variant='contained'>Přihlásit se</Button>
          </div>
        </form>

      </CardContent>
    </Card>
  )
}


export default Login
