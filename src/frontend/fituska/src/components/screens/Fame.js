import React, {useState, useContext, useEffect} from 'react'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { Typography } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { UserContext } from '../../providers/UserContext'
import {FameTable} from '../tables'
import './Fame.css'

const Fame = () => {
  const [course, setCourse] = useState(-1)
  const { sendRequest } = useContext(UserContext)
  const [courses, setCourses] = useState([])
  const fetchCourses = () => {
    const response = sendRequest('GET', 'http://localhost:8000/api/courses/public/', {})
    response.then(res => {
      if(res !== 400)
      {
        setCourses(res)
      }
    })
  }
  useEffect(()=> {
    fetchCourses()
  }, [])
  const handleChange = (e) => {
    setCourse(e.target.value)
  }

  return(
    <div className='window'>
      <Card sx={{margin:'10px'}}>
        <CardContent>
          <div className="title">
            <Typography variant='h3'>
                Síň slávy
            </Typography>
          </div>
          <div className='filter'>
            <div className='filterText'>
              <Typography variant='h6'>
                Zobrazit žebříček pro:
              </Typography>
            </div>
            <div>
              <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                <InputLabel id="demo-simple-select-standard-label">Kurzy</InputLabel>
                <Select
                  labelId="demo-simple-select-standard-label"
                  id="demo-simple-select-standard"
                  value={course}
                  onChange={handleChange}
                  label="Age"
                >
                  <MenuItem value={-1}>Všechny Kurzy</MenuItem>
                  {courses.map(course => (
                    <MenuItem value={course.id}>{course.abbreviation}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </div>
          </div>
        </CardContent>
      </Card>
      <Card sx={{margin:'10px'}}>
        <CardContent>
          <FameTable courseId={course}/>
        </CardContent>
      </Card>
    </div>
  )
}


export default Fame
