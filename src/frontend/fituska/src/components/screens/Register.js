import React, {useState, useContext} from 'react'
import { Card, CardContent, Typography, Button, TextField } from '@mui/material'
import { UserContext } from '../../providers/UserContext'
import { useHistory } from "react-router-dom";
import './Register.css'

const Register = () => {
  const {sendRequest} = useContext(UserContext)
  const history = useHistory()

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [passrepeat, setPassRepeat] = useState('')
  const [passcheck, setPassCheck] = useState(true)
  const [firstname, setFirstName] = useState('')
  const [lastname, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [error, setError] = useState('')

  const handleSubmit = (e) => {
    e.preventDefault();
    const response = sendRequest('POST', 'http://localhost:8000/api/users/', JSON.stringify({
      username: username,
      email: email,
      name: firstname,
      surname: lastname,
      password: password
    }))
    response.then((res) => {
      if(res === 400)
      {
        setError('Zadaná přezdívka nebo email je již registrován')
      }
      else
      {
        if(res.username === username)
        {
          history.push({
            pathname: '/login',
          })
        }
      }
    })
  }

  const handleUserChange = (e) => {
    setUsername(e.target.value)
  }

  const handlePassChange = (e) => {
    setPassword(e.target.value)
    setPassCheck(e.target.value === passrepeat)
  }

  const handlePassRepeatChange = (e) => {
    setPassRepeat(e.target.value)
    setPassCheck(e.target.value === password)
  }

  const handleFnameChange = (e) => {
    setFirstName(e.target.value)
  }

  const handleLnameChange = (e) => {
    setLastName(e.target.value)
  }

  const handleEmailChange = (e) => {
    setEmail(e.target.value)
  }

  return (
    <Card sx={{margin: '10px'}}>
      <CardContent >
        <Typography sx={{color: 'red'}}>{error}</Typography>
        <form onSubmit={handleSubmit}>
            <div>
                <TextField variant="outlined" fullWidth type="text" label="Uživatelské jméno" margin="normal"
                name="username" value={username} onChange={handleUserChange} required/>
            </div>
            <div>
                <TextField variant="outlined" fullWidth type="password" label="Heslo" margin="normal"
                name="password" value={password} onChange={handlePassChange} required/>
            </div>
            <div>
                <TextField variant="outlined" fullWidth type="password" label="Heslo (znovu)" margin="normal"
                name="password" onChange={handlePassRepeatChange} required/>
                <p hidden={passcheck}>Hesla se neshodují</p>
            </div>
            <div>
                <TextField variant="outlined" fullWidth type="text" label="Jméno" margin="normal"
                name="firstname" value={firstname} onChange={handleFnameChange} required/>
            </div>
            <div>
                <TextField variant="outlined" fullWidth type="text" label="Příjmení" margin="normal"
                name="firstname" value={lastname} onChange={handleLnameChange} required/>
            </div>
            <div>
                <TextField variant="outlined" fullWidth type="email" label="Email" margin="normal"
                name="email" value={email} onChange={handleEmailChange} required/>
            </div>
            <div>
                <Button type="submit" variant="contained">Registrovat</Button>
            </div>
        </form>
      </CardContent>
    </Card>
  );
}


export default Register
