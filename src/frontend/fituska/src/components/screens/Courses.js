import React, {useContext, useState} from 'react'
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import { Typography } from '@mui/material';
import {CourseTable} from '../tables'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button'
import { UserContext } from '../../providers/UserContext'
import {AddCourse} from '../modals'
import './Courses.css'

const Courses = () => {
    const {token} = useContext(UserContext)
    const [filterValue, setFilterValue] = useState("all")
    const [isAddCourse, setAddCourse] = useState(false)
    const handleFilterChange = (e) => {
        console.log(e.target.value)
        setFilterValue(e.target.value)
    }
    const handleSetAddCourse = () =>
    {
        setAddCourse(true)
    }
    return (
        <div className='window'>
            {isAddCourse === false ? ''
                :
                <AddCourse isAddCourse={isAddCourse} setAddCourse={setAddCourse} />
            }
            <Card sx={{margin: '10px'}}>
                <CardContent>
                    <div className="title">
                        <Typography variant='h3'>
                            Kurzy
                        </Typography>
                    </div>
                    {token ? 
                        <>
                            <div className='filter'>
                                <FormControl component="fieldset">
                                    <FormLabel component="legend" size="small">Filter</FormLabel>
                                    <RadioGroup row aria-label="Kurzy" size="small" value={filterValue} onChange={handleFilterChange}>
                                        <FormControlLabel value='all' control={<Radio size="small"/>} label="Všechny kurzy" />
                                        <FormControlLabel value='register' control={<Radio size="small"/>} label="Navštěvované kurzy" />
                                        <FormControlLabel value='teach' control={<Radio size="small"/>} label="Vyučované kurzy" />
                                    </RadioGroup>
                                </FormControl>
                            </div>
                            <div>
                                <Button onClick={handleSetAddCourse} variant='contained'>Vytvořit kurz</Button>
                            </div>
                        </>
                        :
                        ''
                    }
                </CardContent>
            </Card>
            <Card sx={{margin: '10px'}}>
                <CardContent>
                    <CourseTable courseFilter={filterValue} isAddCourse={isAddCourse}/>
                </CardContent>
            </Card>
        </div>
    )
}

export default Courses
