import React, {useContext} from 'react'
import { Typography } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import {AcceptEnroledTable, AcceptNewCourses, UserAdminTable} from '../tables'
import { UserContext } from '../../providers/UserContext'
import './Admin.css'

const Admin = () => {
  const {role} = useContext(UserContext)
  return(
    <div className='window'>
      <Card sx={{margin:'10px', width:'100%'}}>
        <CardContent>
          <div className="title">
            <Typography variant='h4'>
              Schválení studentů do kurzů
            </Typography>
          </div>
          <AcceptEnroledTable/>
        </CardContent>
      </Card>
      {role !== 'standard' ? 
        <Card sx={{margin:'10px', width:'100%'}}>
          <CardContent>
            <div className="title">
              <Typography variant='h4'>
                Schválení nových kurzů
              </Typography>
            </div>
            <AcceptNewCourses/>
          </CardContent>
        </Card>
        :
        ''
      }
      {role === 'admin' ? 
        <Card sx={{margin:'10px', width:'100%'}}>
          <CardContent>
            <div className="title">
              <Typography variant='h4'>
                Správa uživatelů
              </Typography>
            </div>
            <UserAdminTable/>
          </CardContent>
        </Card>
        :
        ''
      }
    </div>
  )
}


export default Admin
