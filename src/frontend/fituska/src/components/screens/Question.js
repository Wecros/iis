import React, {useState, useEffect, useContext} from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { Typography } from '@mui/material';
import {AnswerCard} from '../cards'
import TextareaAutosize from '@mui/material/TextareaAutosize';
import Button from '@mui/material/Button';
import moment from 'moment'
import {useParams} from 'react-router-dom'
import {Thread} from '../modals'
import { UserContext } from '../../providers/UserContext'
import './Question.css'

const Question = props => {

  const {token, sendRequest, role} = useContext(UserContext)
  const [data, setData] = useState({})

  const [error, setError] = useState('')

  const params = useParams()

  const [isTeacher, setTeacher] = useState(false)
  const [isStudentEnrolled, setStudentEnrolled] = useState(false)
  const [isStudentPending, setStudentPending] = useState(false)
  console.log(isStudentEnrolled)
  const fetchTeacher = (data) => {
    const response = sendRequest('GET', 'http://localhost:8000/api/me/courses/teach', {})
    response.then(res => {
      if(res !== 400)
      {
        res.forEach((course) => {
          if(course.id === parseInt(params.course_id))
          {
            setTeacher(true)
          }
        })
      }
    })
  }

  const fetchEnrolledStudent = () =>
  {
    const response = sendRequest('GET', 'http://localhost:8000/api/me/courses/registered/', {})
    response.then(res => {
      res.forEach(element => {
        console.log(element.id, parseInt(params.course_id))
        if (element.id === parseInt(params.course_id))
        {
          setStudentEnrolled(true)
        }
      })
    })
  }

  const fetchPendingStudent = () =>
  {
    const response = sendRequest('GET', 'http://localhost:8000/api/me/courses/pending_registration/', {})
    response.then(res => {
      res.forEach(element => {
        if (element.id === parseInt(params.courseId))
        {
          setStudentPending(true)
        }
      })
    })
  }

  const readQuestions = () => {
    const response = sendRequest('GET', 'http://localhost:8000/api/categories/' + params.category_id + '/questions', {})
    response.then(res => {
      res.forEach(element => {
        if (element.id === parseInt(params.question_id))
        {
          setData(element)
          if(token)
          {
            fetchTeacher(element)
            fetchEnrolledStudent()
            fetchPendingStudent()
          }
        }
      })
    })
  }


  useEffect(()=> {
    readQuestions()
  }, [sendRequest, token, params])


  const handleLike = (id) => {
    const body = {
    }
    const response = sendRequest('POST', 'http://localhost:8000/api/answers/' + id + '/reactions', JSON.stringify(body))
    response.then(res => {
      readQuestions()
    })
  }
  const [threadId, setThreadId] = useState(-1)
  const [questionEndState, setQuestionEndState] = useState(0)
  const [checkedAnswers, setCheckedAnswers] = useState([])
  const [answersLikes, setAnswersLikes] = useState([])

  const handleThread = (id) =>
  {
    setThreadId(id)
  }

  const handleQuestionEndStart = () => {
    setQuestionEndState(1)
  }

  const [finalAnswerText, setFinalAnswerText] = useState('')
  const [finalAnswerError, setFinalAnswerError] = useState('')

  const handleFinnalAnswer = (e) => {
    setFinalAnswerText(e.target.value)
  }

  const handleQuestionEndEnd = () => {
    if(finalAnswerText.length > 0)
    {
      setQuestionEndState(0)
      checkedAnswers.forEach(answerId => {
        let currAnswersLike = answersLikes
        const index = currAnswersLike.map((item) => item.id).indexOf(answerId)
        let likeCnt = 0
        if(index > -1)
        {
          likeCnt = currAnswersLike[index].value
        }
        const response = sendRequest('PUT', 'http://localhost:8000/api/answers/' + answerId + '?extra_votes=' + likeCnt, {})
        response.then(res => {
          console.log(res)
          setCheckedAnswers([])
          setAnswersLikes([])
          setFinalAnswerError('')
        })
      })
      const body = {
        text: finalAnswerText,
      }
      const response = sendRequest('PUT', 'http://localhost:8000/api/questions/' + params.question_id, JSON.stringify(body))
      response.then(res => {
        console.log(res)
        setCheckedAnswers([])
        setAnswersLikes([])
        setFinalAnswerError('')
        readQuestions()
      })
    }
    else {
      setFinalAnswerError('Odpověď nemůže být prázdná')
    }
  }

  const handleQuestionEndCancel = () => {
    setQuestionEndState(0)
    setCheckedAnswers([])
    setAnswersLikes([])
    setFinalAnswerError('')
  }

  const handleSelectAnswer = (id) =>
  {
    let currState = checkedAnswers
    const index = currState.indexOf(id)
    if(index > -1)
    {
      currState.splice(index, 1)
    }
    else {
      currState.push(id)
    }
    setCheckedAnswers(currState)
  }

  const handleAnswersLikes = (e, id) =>
  {
    let currState = answersLikes
    const index = currState.map((item) => item.id).indexOf(id)
    if(index > -1)
    {
      currState[index].value = e.target.value
    }
    else {
      currState.push({id: id, value: e.target.value})
    }
    setAnswersLikes(currState)
  }

  const [questionText, setQuestionText] = useState('')

  const handleQuestionText = (e) =>
  {
    setQuestionText(e.target.value)
  }

  const handleQuestionSend = () =>
  {
    const body = {
      text: questionText
    }
    const response = sendRequest('POST', 'http://localhost:8000/api/questions/' + params.question_id + '/answers', JSON.stringify(body))
    response.then(res => {
      if(res === 403)
      {
        setError('Uživatel, který vytvořil otázku, nemůže napsat odpověď')
      }
      readQuestions()
      setQuestionText('')
    })
  }


  return (
    <>
      {threadId === -1 ? ''
        :
        <Thread threadId={threadId} setThreadId={setThreadId} message={data.answers.find(message => message.id === threadId)}/>
      }
      <div className='card'>
        <Card sx={{width: '100%'}}>
          <CardContent>
            <div className='window'>
              <div className="title">
                  <Typography variant='h6'>
                      Založil: {data?.author?.name} {data?.author?.surname}
                  </Typography>
                  <Typography variant='h6'>
                    Datum: {moment(data.created_at).format('h:mm:ss DD/MM/YYYY')}
                  </Typography>
                  {data.is_checked ?
                    <Typography variant='caption'>
                      Otázka byla uzavřena
                    </Typography>
                    :
                    ''
                  }
              </div>
              <div>
                {
                  data.is_checked ? ''
                    :
                    isTeacher ?
                      questionEndState === 1 ?
                        <>
                          <Button variant='contained'sx={{marginLeft:'10px', backgroundColor:'green'}} onClick={handleQuestionEndEnd}>Potvrdit uzavření</Button>
                          <Button variant='contained'sx={{marginLeft:'10px', backgroundColor:'red'}} onClick={handleQuestionEndCancel}>Zrušit uzavření</Button>
                        </>
                        :
                        <Button variant='contained'sx={{marginLeft:'10px'}} onClick={handleQuestionEndStart}>Uzavřít otázku</Button>
                      :
                      ''
                }
              </div>
              <div className='card'>
                <Card sx={{maxWidth: '1000px', width: '100%'}}>
                  <CardContent sx={{padding: '5px'}}>
                    <div className='question'>
                      <div className='item'>
                        <Typography variant='h6'>
                          {data?.name}
                        </Typography>
                        <Typography variant='caption'>
                          {data?.details}
                        </Typography>
                      </div>
                      <div className='dataItem'>
                      </div>
                    </div>
                  </CardContent>
                </Card>
              </div>
              {data.answers ? data.answers.map((data) =>(
                  <AnswerCard
                    data={data}
                    key={data.id}
                    handleLike={handleLike}
                    handleThread={handleThread}
                    evaluateAnswer={questionEndState}
                    handleCheck={handleSelectAnswer}
                    handleLikes={handleAnswersLikes}
                  />
                ))
                :
                ''
              }
              {
                data.is_checked || !token ? ''
                  :
                  !isTeacher && isStudentEnrolled ?
                    <div className='newQuestion'>
                      <Card>
                        <CardContent>
                          <Typography sx={{color: 'red'}}>{error}</Typography>
                          <div className='questionFlex'>
                            <Typography>Zadejte odpoveď</Typography>
                            <TextareaAutosize
                              aria-label="minimum height"
                              minRows={4}
                              style={{ width: '100%' }}
                              onChange={handleQuestionText}
                              value={questionText}
                            />
                          </div>
                          <Button variant="contained" onClick={handleQuestionSend}>Odeslat</Button>
                        </CardContent>
                      </Card>
                    </div>
                    :
                    ''
              }
              {
                isTeacher && questionEndState ?
                  <div className='newQuestion'>
                    <Card>
                      <CardContent>
                        <div className='questionFlex'>
                          <Typography>Zadejte konkrétní odpověď</Typography>
                          <Typography sx={{color: 'red'}}>{finalAnswerError}</Typography>
                          <TextareaAutosize
                            aria-label="minimum height"
                            minRows={4}
                            style={{ width: '100%' }}
                            onChange={handleFinnalAnswer}
                            required
                            value={finalAnswerText}
                          />
                        </div>
                      </CardContent>
                    </Card>
                  </div>
                :
                ''
              }
            </div>
          </CardContent>
        </Card>
      </div>
    </>
  )
}


export default Question
