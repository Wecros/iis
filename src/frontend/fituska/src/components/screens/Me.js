import React, {useState, useEffect, useContext} from 'react'
import {Card, CardContent, TextField} from '@mui/material'
import { UserContext } from '../../providers/UserContext'

const Me = () => {
  const {sendRequest} = useContext(UserContext)
  const [username, setUsername] = useState('')
  const [firstname, setFirstName] = useState('')
  const [lastname, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [userRole, setUserRole] = useState('')
  const [karma, setKarma] = useState('')
  useEffect(()=> {
    const fetchMe = () => {
      const response = sendRequest('GET', 'http://localhost:8000/api/me/', {})
      response.then(res => {
        if(res !== 401)
        {
          setUsername(res.username)
          setFirstName(res.name)
          setLastName(res.surname)
          setEmail(res.email)
          setUserRole(res.role)
          setKarma(res.karma)
        }
      })
    }
    fetchMe()
  }, [sendRequest])
  return(
    <div>
        <Card sx={{margin: '10px'}}>
          <CardContent>
            <div>
              <TextField variant="outlined" fullWidth type="text" label="Uživatelské jméno" margin="normal"
              name="username" value={username} />
            </div>
            <div>
              <TextField variant="outlined" fullWidth type="text" label="Jméno" margin="normal"
              name="firstname" value={firstname} />
            </div>
            <div>
              <TextField variant="outlined" fullWidth type="text" label="Příjmení" margin="normal"
              name="firstname" value={lastname}/>
            </div>
            <div>
              <TextField variant="outlined" fullWidth type="email" label="Email" margin="normal"
              name="email" value={email}/>
            </div>
            <div>
              <TextField variant="outlined" fullWidth type="text" label="Role" margin="normal"
              name="email" value={userRole}/>
            </div>
            <div>
              <TextField variant="outlined" fullWidth type="text" label="Karma" margin="normal"
              name="email" value={karma}/>
            </div>
          </CardContent>
        </Card>
    </div>
)}

export default Me
