import React from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { CardActionArea } from '@mui/material';
import { Typography } from '@mui/material';
import { useHistory } from "react-router-dom";
import './QuestionCard.css'

const QuestionCard = ({data, courseId}) => {
  console.log(data)
  const history = useHistory()
  const handleClick = () => {
    history.push({
      pathname: '/question/' + data.category_id + '/' + data.id + '/' + courseId,
    })
  }
  return (
    <div className='card'>
      <Card>
        <CardActionArea onClick={handleClick}>
          <CardContent sx={{padding: '5px'}}>
            <div className='question'>
              <div className='item'>
                <Typography variant='h6'>
                  {data.name}
                </Typography>
                <Typography variant='caption' sx={{color: '#6d6d6d'}}>
                  {
                    data.details.length > 40 ? 
                      data.details.substring(0, 40) + '...'
                      :
                      data.details
                  }
                </Typography>
              </div>
              <div className='icons'>
                <div className='dataItem'>
                  <div>
                    {data.answers.length}
                  </div>
                  <div>
                    odpovedí
                  </div>
                </div>
                <div className='dataItem'>
                  <div>
                    Založil:
                  </div>
                  <div>
                    {data.author.username}
                  </div>
                </div>
              </div>
            </div>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  )
}

export default QuestionCard
