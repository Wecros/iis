import React, {useContext} from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { Typography, TextField } from '@mui/material';
import moment from 'moment'
import MessageIcon from '@mui/icons-material/Message';
import ThumbUpIcon from '@mui/icons-material/ThumbUp';
import Checkbox from '@mui/material/Checkbox';
import { UserContext } from '../../providers/UserContext'
import './AnswerCard.css'

const AnswerCard = ({data, handleLike, handleThread, evaluateAnswer, handleCheck, handleLikes}) => {
  const {token} = useContext(UserContext)
  console.log(data)
  return (
    <div className='card'>
      <Card>
          <CardContent sx={{padding: '5px', backgroundColor: data.is_correct ? 'green': 'white',}}>
            <div className='question'>
              <div className='item'>
                <div className='header'>
                  <div>
                    <Typography variant='caption'>
                      Od: <b>{data.author.username}</b>
                    </Typography>
                  </div>
                  <div>
                    <Typography variant='caption'>
                      {moment(data.created_at).format('h:mm:ss DD/MM/YYYY')}
                    </Typography>
                  </div>
                </div>
                <div>
                  <Typography variant='caption'>
                    {data.text}
                  </Typography>
                </div>
              </div>
              {token ?
                <div className='icons'>
                  {handleLike ?
                    <div className='dataItem' style={{paddingTop: '12px', cursor: 'pointer'}} onClick={() => handleLike(data.id)}>
                      <ThumbUpIcon />
                      <Typography variant='caption'>{data.reactions.length}</Typography>
                    </div>
                    :
                    ''
                  }
                  {handleThread ?
                    <div className='dataItem' style={{cursor: 'pointer'}} onClick={() => handleThread(data.id)}>
                      <MessageIcon />
                    </div>
                    :
                    ''
                  }
                  {
                    evaluateAnswer ?
                      <>
                        <div className='dataItem'>
                          <TextField
                            id="arrayLen"
                            label="Přidat hlasy"
                            variant="standard"
                            type="number"
                            size='small'
                            style={{width: 100}}
                            onChange={(e) => handleLikes(e, data.id)}
                          />
                        </div>
                        <div className='dataItem'>
                          <Checkbox onChange={() => handleCheck(data.id)} />
                        </div>
                      </>
                    :
                      ''
                  }
                </div>
                :
                ''
              }
            </div>
          </CardContent>
      </Card>
    </div>
  )
}

export default AnswerCard
