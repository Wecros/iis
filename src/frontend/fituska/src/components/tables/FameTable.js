import React, {useContext, useState, useEffect} from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { UserContext } from '../../providers/UserContext'
import {FameRow} from '.';
import './FameTable.css';

const FameTable = ({courseId}) => {
  const { sendRequest } = useContext(UserContext)
  const [data, setData] = useState([])
  const fetchFame = () => {
    const response = sendRequest('GET', 'http://localhost:8000/api/fame/', {})
    response.then(res => {
      if(res !== 400)
      {
        console.log(courseId)
        if(courseId === -1)
        {
          setData(res.all_courses)
        }
        else {
          const index = res.individual_courses.map((item) => item.course.id).indexOf(courseId)
          if(index > -1)
          {
            setData(res.individual_courses[index].top_users)
          }
        }
      }
    })
  }
  useEffect(()=> {
    fetchFame()
  }, [courseId])

  return (
    <div>
      <TableContainer>
        <Table
          sx={{
            minWidth: 650,
            '& .MuiTableHead-root': {
                backgroundColor: '#1976d2',
            },
          }}
          aria-label="simple table"
        >
          <TableHead sx={{'& .MuiTableCell-head': {color: 'white'}}}>
            <TableRow>
              <TableCell align="left">Uživatelské jméno</TableCell>
              <TableCell align="left">Jméno</TableCell>
              <TableCell align="center">Nasbírané hlasy</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((row, idx) => (
              <FameRow data={row} key={idx}/>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default FameTable
