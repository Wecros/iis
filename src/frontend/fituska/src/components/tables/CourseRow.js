import React from 'react';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import { useHistory } from "react-router-dom";

const CourseRow = (data) => {
  const history = useHistory()
  const handleClick = () => {
    history.push({
      pathname: '/course/' + data.data.id,
      data: data.data,
    })
  }
  return (
    <TableRow
      key={data.data.name}
      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
      onClick={handleClick}
      hover
    >
      <TableCell component="th" scope="row">
        {data.data.abbreviation}
      </TableCell>
      <TableCell align="left">{data.data.name}</TableCell>
      <TableCell align="left">{data.data.teacher.name} {data.data.teacher.surname}</TableCell>
      <TableCell align="center">{data.data.registered_count}</TableCell>
      <TableCell align="center">{data.data.question_count}</TableCell>

    </TableRow>
  )
}

export default CourseRow
