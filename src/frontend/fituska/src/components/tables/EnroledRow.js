import * as React from 'react';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';



const EnroledRow = ({data, handleAccept, handleReject, toggleCheck, isChecked}) => {
  return (
      <TableRow
        key={data.id}
        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
      >
        <TableCell component="th" scope="row">
          <Checkbox checked={isChecked} onClick={() => toggleCheck(data.id)}/>
        </TableCell>

        <TableCell align="right">{data.name} {data.surname}</TableCell>
        <TableCell align="right">{data.email}</TableCell>
        <TableCell align="right">{data.username}</TableCell>
        <TableCell align="right">
          <Button variant="contained" sx={{backgroundColor: 'green'}} onClick={() => handleAccept(data.id)}>
            Potvrdit
          </Button>
        </TableCell>
        <TableCell align="right">
          <Button variant="contained" sx={{backgroundColor: 'red'}} onClick={() => handleReject(data.id)}>
            Zamítnout
          </Button>
        </TableCell>
      </TableRow>
  )
}

export default EnroledRow
