import React, {useState, useEffect, useContext} from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import { UserContext } from '../../providers/UserContext'
import {UnconfirmedCourseRow} from '.';
import './AcceptNewCourses.css';

const AcceptNewCourses = () => {
  const {sendRequest} = useContext(UserContext)

  const [courses, setCourses] = useState([])

  const fetchUnconfirmedCourses = () => {
    const unconfirmedCourses = []
    const data = sendRequest('GET', 'http://localhost:8000/api/courses/private/', {})
    data.then(res => {
      if(res !== 400)
      {
        res.forEach(course => {
          if(course.approved === false){
            unconfirmedCourses.push(course)
          }
        })
        setCourses(unconfirmedCourses)
      }
    })
  }

  useEffect(()=> {
    fetchUnconfirmedCourses()
  }, [])

  const acceptCourse = (id) => {
    const data = sendRequest('PUT', 'http://localhost:8000/api/courses/approve/' + id , {})
    data.then(res => {
      console.log(res)
      fetchUnconfirmedCourses()
    })
  }

  const deleteCourse = (id) => {
    const data = sendRequest('DELETE', 'http://localhost:8000/api/courses/' + id , {})
    data.then(res => {
      console.log(res)
      fetchUnconfirmedCourses()
    })
  }

  const [checkedState, setChecked] = useState([])

  const handleAccept = (id) => {
    acceptCourse(id)
  }

  const handleReject = (id) => {
    deleteCourse(id)
  }

  const toggleCheck = (id) => {
    let currState = checkedState
    const index = currState.indexOf(id)
    if(index > -1)
    {
      currState.splice(index, 1)
    }
    else {
      currState.push(id)
    }
    setChecked(currState)
  }

  const handleCheckedAccept = () => {
    checkedState.forEach((id) =>{
      acceptCourse(id)
    })
    setChecked([])
  }

  const handleCheckedReject = () => {
    checkedState.forEach((id) =>{
      deleteCourse(id)
    })
    setChecked([])
  }

  return (
    <div>
      <div className="title">
        <Button variant="contained" sx={{backgroundColor: 'green', marginRight: '5px'}} onClick={handleCheckedAccept}>
          Potvrdit označených
        </Button>
        <Button variant="contained" sx={{backgroundColor: 'red'}} onClick={handleCheckedReject}>
          Zamítnout označených
        </Button>
      </div>
      <TableContainer>
        <Table
          sx={{
            '& .MuiTableHead-root': {
                backgroundColor: '#1976d2',
            },
          }}
          aria-label="simple table"
        >
          <TableHead sx={{'& .MuiTableCell-head': {color: 'white'}}}>
            <TableRow>
              <TableCell align="center">Označit</TableCell>
              <TableCell align="center">Zkratka</TableCell>
              <TableCell align="center">Vyučující</TableCell>
              <TableCell align="center">Přijat</TableCell>
              <TableCell align="center">Odmítnut</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {courses.map((row, idx) => (
              <UnconfirmedCourseRow data={row} toggleCheck={toggleCheck} handleAccept={handleAccept} handleReject={handleReject} key={idx}/>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default AcceptNewCourses
