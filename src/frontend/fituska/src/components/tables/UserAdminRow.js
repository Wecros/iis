import * as React from 'react';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';



const UserAdminRow = ({data, handleDelete, handleEdit, toggleCheck, isChecked}) => {

  return (
      <TableRow
        key={data.id}
        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
      >
        <TableCell component="th" scope="row">
          {data.is_active ?
            <Checkbox checked={isChecked} onClick={() => toggleCheck(data.id)}/>
            :
            ''
          }
        </TableCell>

        <TableCell align="left">{data.username}</TableCell>
        <TableCell align="left">{data.name} {data.surname}</TableCell>
        <TableCell align="center">{data.role}</TableCell>
        <TableCell align="right">
          <Button variant="contained" onClick={() => handleEdit(data.id)}>
            Upravit
          </Button>
        </TableCell>
        <TableCell align="center">
          {data.is_active ?
            <Button variant="contained" sx={{backgroundColor: 'red'}} onClick={() => handleDelete(data.id)}>
              Deaktivovat
            </Button>
            :
            'Deaktivovaný'
          }
        </TableCell>
      </TableRow>
  )
}

export default UserAdminRow
