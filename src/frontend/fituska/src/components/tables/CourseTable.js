import React, {useContext, useEffect, useState} from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { UserContext } from '../../providers/UserContext';
import {CourseRow} from '.';
import './CourseTable.css';

const CourseTable = ({courseFilter, isAddCourse}) => {
  const [fetchedData, setFetchedData] = useState([])
  const {token, sendRequest} = useContext(UserContext)

  const fetchCourses = () => {
    let request = ''
    switch(courseFilter) {
      case 'all':
        request = 'http://localhost:8000/api/courses/public/'
        break
      case 'register':
        request = 'http://localhost:8000/api/me/courses/registered/'
        break
      case 'teach':
        request = 'http://localhost:8000/api/me/courses/teach/'
        break
      case 'pending':
        request = 'http://localhost:8000/api/me/courses/pending_registration/'
        break
      default:
        break
    }
    const data = sendRequest('GET', request, {})
    data.then(res => {
      if(res !== 400)
      {
        console.log(res)
        setFetchedData(res)
      }
    })
  }

  useEffect(()=> {
    fetchCourses()
  }, [sendRequest, token, courseFilter])

  return (
    <div>
      <TableContainer>
        <Table
          sx={{
            minWidth: 650,
            '& .MuiTableHead-root': {
                backgroundColor: '#1976d2',
            },
          }}
          aria-label="simple table"
        >
          <TableHead sx={{'& .MuiTableCell-head': {color: 'white'}}}>
            <TableRow>
              <TableCell align="center">Zkratka</TableCell>
              <TableCell align="left">Název</TableCell>
              <TableCell align="center">Vedoucí</TableCell>
              <TableCell align="center">Počet přihlášených</TableCell>
              <TableCell align="center">Počet otázek</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {fetchedData ? fetchedData.map((row, idx) => (
              <CourseRow data={row} key={idx} />
            ))
              : ''
            }
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default CourseTable
