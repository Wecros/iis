import React, {useState, useEffect, useContext} from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import { FormControl, InputLabel, Select, MenuItem } from '@mui/material';
import { UserContext } from '../../providers/UserContext'
import {EnroledRow} from '.';
import './AcceptEnroledTable.css';

const AcceptEnroledTable = () => {
  const {sendRequest, role} = useContext(UserContext)

  const [checkedState, setChecked] = useState([])
  const [course, setCourse] = useState('')
  const [taughtCourses, setTaughtCourses] = useState([])

  const fetchTaughtCourses = () => {
    let data
    if(role === 'standard')
    {
      data = sendRequest('GET', 'http://localhost:8000/api/me/courses/teach/', {})
    }
    else {
      data = sendRequest('GET', 'http://localhost:8000/api/courses/public/', {})
    }
    data.then(res => {
      if(res !== 400)
      {
        setTaughtCourses(res)
      }
    })
  }

  useEffect(()=> {
    fetchTaughtCourses()
  }, [])

  const [pendingStudents, setPendingStudents] = useState([])

  const fetchPendingStudents = () =>
  {
    if(course !== '')
    {
      const data = sendRequest('GET', 'http://localhost:8000/api/courses/' + course + '/pending_registrations', {})
      data.then(res => {
        if(res !== 400)
        {
          setPendingStudents(res)
        }
      })
    }
  }

  useEffect(()=> {
    fetchPendingStudents()
  }, [course])

  const acceptUserToCourse = (userId) => {
    const data = sendRequest('PUT', 'http://localhost:8000/api/courses/' + course + '/registration/' + userId + '/approve/', {})
    data.then(res => {
      fetchPendingStudents()
    })
  }

  const declineUserToCourse = (userId) => {
    const data = sendRequest('PUT', 'http://localhost:8000/api/courses/' + course + '/registration/' + userId + '/disapprove/', {})
    data.then(res => {
      fetchPendingStudents()
    })
  }


  const handleSetCourse = (e) =>
  {
    setCourse(e.target.value)
  }

  const handleAccept = (id) => {
    acceptUserToCourse(id)
  }

  const handleReject = (id) => {
    declineUserToCourse(id)
  }

  const toggleCheck = (id) => {
    let currState = checkedState
    const index = currState.indexOf(id)
    if(index > -1)
    {
      currState.splice(index, 1)
    }
    else {
      currState.push(id)
    }
    setChecked(currState)
    console.log(currState)
  }

  const handleCheckedAccept = () => {
    checkedState.forEach(userId => {
      acceptUserToCourse(userId)
    })
  }

  const handleCheckedReject = () => {
    checkedState.forEach(userId => {
      declineUserToCourse(userId)
    })
  }

  return (
    <div>
      <div className="title">
        <Button variant="contained" sx={{backgroundColor: 'green', marginRight: '5px'}} onClick={handleCheckedAccept}>
          Potvrdit označených
        </Button>
        <Button variant="contained" sx={{backgroundColor: 'red'}} onClick={handleCheckedReject}>
          Zamítnout označených
        </Button>
      </div>
      <div>
        <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
          <InputLabel id="demo-simple-select-standard-label">Kurzy</InputLabel>
          <Select
            labelId="demo-simple-select-standard-label"
            id="demo-simple-select-standard"
            value={course}
            onChange={handleSetCourse}
          >
            {taughtCourses.map((course, idx) => (
              <MenuItem value={course.id} key={idx} >{course.abbreviation}</MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
      <TableContainer>
        <Table
          sx={{
            '& .MuiTableHead-root': {
                backgroundColor: '#1976d2',
            },
          }}
          aria-label="simple table"
        >
          <TableHead sx={{'& .MuiTableCell-head': {color: 'white'}}}>
            <TableRow>
              <TableCell align="center">Označit</TableCell>
              <TableCell align="center">Student</TableCell>
              <TableCell align="center">Email</TableCell>
              <TableCell align="center">Login</TableCell>
              <TableCell align="center">Přijat</TableCell>
              <TableCell align="center">Odmítnut</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {pendingStudents.map((row) => (
              <EnroledRow data={row} toggleCheck={toggleCheck} handleAccept={handleAccept} handleReject={handleReject} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default AcceptEnroledTable
