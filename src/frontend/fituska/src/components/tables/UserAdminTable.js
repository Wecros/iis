import React, {useState, useEffect, useContext} from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from '@mui/material/Button';
import { UserContext } from '../../providers/UserContext'
import {UserAdminRow} from '.';
import {UserEdit} from '../modals'
import './UserAdminTable.css';

const UserAdminTable = () => {
  const { sendRequest } = useContext(UserContext)

  const [users, setUsers] = useState([])

  const fetchUsers = () => {
    const data = sendRequest('GET', 'http://localhost:8000/api/users/private/', {})
    data.then(res => {
      if(res !== 400)
      {
        setUsers(res)
      }
    })
  }


  const deleteUser = (id) => {
    const data = sendRequest('DELETE', 'http://localhost:8000/api/users/' + id, {})
    data.then(res => {
      fetchUsers()
    })
  }


  const [checkedState, setChecked] = useState([])
  const [editId, setEditId] = useState(-1)

  const handleDelete = (id) => {
    deleteUser(id)
  }

  const handleEdit = (id) => {
    setEditId(id)
  }

  const toggleCheck = (id) => {
    let currState = checkedState
    const index = currState.indexOf(id)
    if(index > -1)
    {
      currState.splice(index, 1)
    }
    else {
      currState.push(id)
    }
    setChecked(currState)
  }

  const handleCheckedDelete = () => {
    checkedState.forEach((userId) =>{
      deleteUser(userId)
      fetchUsers()
    })
  }

  useEffect(()=> {
    fetchUsers()
  }, [editId])

  return (
    <div>
      {editId === -1 ? ''
        :
        <UserEdit editId={editId} setEditId={setEditId}/>
      }
      <div className="title">
        <Button variant="contained" sx={{backgroundColor: 'red', marginRight: '5px'}} onClick={handleCheckedDelete}>
          Deaktivovat označených
        </Button>
      </div>
      <div>
        <TableContainer>
          <Table
            sx={{
              '& .MuiTableHead-root': {
                  backgroundColor: '#1976d2',
              },
            }}
            aria-label="simple table"
          >
            <TableHead sx={{'& .MuiTableCell-head': {color: 'white'}}}>
              <TableRow>
                <TableCell align="center">Označit</TableCell>
                <TableCell align="center">Login</TableCell>
                <TableCell align="center">Meno</TableCell>
                <TableCell align="center">Role</TableCell>
                <TableCell align="center">Upravit</TableCell>
                <TableCell align="center">Odstranit</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map((row, idx) => (
                <UserAdminRow data={row} toggleCheck={toggleCheck} handleDelete={handleDelete} handleEdit={handleEdit} key={idx} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </div>
  )
}

export default UserAdminTable
