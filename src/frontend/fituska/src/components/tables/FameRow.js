import * as React from 'react';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';

const FameRow = (data) => {
  return (
      <TableRow
        key={data.data.userName}
        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
      >
        <TableCell component="th" scope="row" align="left">
          {data.data.username}
        </TableCell>
        <TableCell align="left">{data.data.name} {data.data.surname}</TableCell>
        <TableCell align="center">{data.data.karma}</TableCell>
      </TableRow>
  )
}

export default FameRow
