import React, {useState, useContext} from 'react'
import {Button, TextField} from '@mui/material';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import { UserContext } from '../../providers/UserContext'
import './AddCourse.css'

const AddCourse = ({isAddCourse, setAddCourse}) => {
  const {sendRequest} = useContext(UserContext)

  const [name, setName] = useState('')
  const [abbreviation, setAbbreviation] = useState('')
  const [description, setDescription] = useState('')

  const handleNameChange = (e) => {
    setName(e.target.value)
  }

  const handleAbbreviationChange = (e) => {
    setAbbreviation(e.target.value)
  }

  const handleDescriptionChange = (e) => {
    setDescription(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const body = {
      name: name,
      abbreviation: abbreviation,
      description: description,
    }
    const response = sendRequest('POST', 'http://localhost:8000/api/me/courses/', JSON.stringify(body))
    response.then(res => {
      console.log(res)
      setAddCourse(false)
    })
  }


  return (
    <div id="myModal" className="modal" style={isAddCourse === false ? {display:'none'}: {display:'block'}}>
      <div className="modal-content">
        <span className="close" onClick={()=>setAddCourse(false)}>&times;</span>
        <p>Přidat kurz</p>
        <div className='modalThread'>
          <form onSubmit={handleSubmit}>
              <div>
                  <TextField variant="outlined" fullWidth type="text" label="Názov" margin="normal"
                  name="name" value={name} onChange={handleNameChange} required/>
              </div>
              <div>
                  <TextField variant="outlined" fullWidth type="text" label="Zkratka" margin="normal"
                  name="firstname" value={abbreviation} onChange={handleAbbreviationChange} required/>
              </div>
              <div>
                <TextareaAutosize placeholder="Popis" margin="normal"
                  minRows={4} style={{ width: '100%' }}
                  name="firstname" value={description} onChange={handleDescriptionChange} required/>
              </div>
              <div id="subButDiv">
                  <Button type="submit" variant="contained">Přidat</Button>
              </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AddCourse
