import React, {useState, useContext, useEffect} from 'react'
import {Button, MenuItem, TextField} from '@mui/material'
import { FormControl, InputLabel, Select } from '@mui/material'
import { UserContext } from '../../providers/UserContext'
import './UserEdit.css'

const UserEdit = ({editId, setEditId}) => {
  const {sendRequest, role} = useContext(UserContext)

  const [username, setUsername] = useState('')
  const [firstname, setFirstName] = useState('')
  const [lastname, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [userRole, setUserRole] = useState('')
  const [karma, setKarma] = useState('')

  const fetchUser = () => {
    const data = sendRequest('GET', 'http://localhost:8000/api/users/' + editId, {})
    data.then(res => {
      setUsername(res.username)
      setFirstName(res.name)
      setLastName(res.surname)
      setEmail(res.email)
      setUserRole(res.role)
      setKarma(res.karma)
    })
  }
  useEffect(()=> {
    fetchUser()
  }, [])

  const handleUserChange = (e) => {
    setUsername(e.target.value)
  }

  const handleFnameChange = (e) => {
    setFirstName(e.target.value)
  }

  const handleLnameChange = (e) => {
    setLastName(e.target.value)
  }

  const handleEmailChange = (e) => {
    setEmail(e.target.value)
  }

  const handleKarmaChange = (e) => {
    setKarma(e.target.value)
  }

  const handleUserRoleChange = (e) => {
    setUserRole(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const body = {
      username: username,
      email: email,
      name: firstname,
      surname: lastname,
      role: userRole,
      karma: karma
    }
    const data = sendRequest('PUT', 'http://localhost:8000/api/users/' + editId, JSON.stringify(body))
    data.then(res => {
      console.log(res)
    })
    setEditId(-1)
  }


  return (
    <div id="myModal" className="modal" style={editId === -1 ? {display:'none'}: {display:'block'}}>
      <div className="modal-content">
        <span className="close" onClick={()=>setEditId(-1)}>&times;</span>
        <p>Upravit uživatela</p>
        <div className='modalThread'>
          <form onSubmit={handleSubmit}>
              <div>
                  <TextField variant="outlined" fullWidth type="text" label="Uživatelské jméno" margin="normal"
                  name="username" value={username} onChange={handleUserChange} required/>
              </div>
              <div>
                  <TextField variant="outlined" fullWidth type="text" label="Jméno" margin="normal"
                  name="firstname" value={firstname} onChange={handleFnameChange} required/>
              </div>
              <div>
                  <TextField variant="outlined" fullWidth type="text" label="Příjmení" margin="normal"
                  name="firstname" value={lastname} onChange={handleLnameChange} required/>
              </div>
              <div>
                  <TextField variant="outlined" fullWidth type="email" label="Email" margin="normal"
                  name="email" value={email} onChange={handleEmailChange} required/>
              </div>
              <div>
                  <TextField variant="outlined" fullWidth type="number" label="Karma" margin="normal"
                  name="email" value={karma} onChange={handleKarmaChange} required/>
              </div>
              <div>
                <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                  <InputLabel id="demo-simple-select-standard-label">Kategórie</InputLabel>
                  <Select
                    labelId="demo-simple-select-standard-label"
                    id="demo-simple-select-standard"
                    value={userRole}
                    onChange={handleUserRoleChange}
                    label="Age"
                  >
                    <MenuItem value="admin">Admin</MenuItem>
                    <MenuItem value="moderator">Moderator</MenuItem>
                    <MenuItem value="standard">Standard</MenuItem>
                  </Select>
                </FormControl>
              </div>
              <div id="subButDiv">
                  <Button type="submit" variant="contained">Uložit</Button>
              </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default UserEdit
