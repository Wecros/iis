import React, {useState, useContext} from 'react'
import {Button, TextField} from '@mui/material';
import { UserContext } from '../../providers/UserContext'
import './AddCategory.css'

const AddCategory = ({isAddCategory, setAddCategory, id}) => {
  const [categoryName, setCategoryName] = useState('')
  const {sendRequest} = useContext(UserContext)

  const handleCategoryChange = (e) => {
    setCategoryName(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const data = sendRequest('POST', 'http://localhost:8000/api/courses/' + id + '/categories', JSON.stringify({name: categoryName}))
    data.then(res => console.log(res))
    setAddCategory(false)
  }

  return (
    <div id="myModal" className="modal" style={isAddCategory === false  ? {display:'none'}: {display:'block'}}>
      <div className="modal-content">
        <span className="close" onClick={()=>setAddCategory(false)}>&times;</span>
        <p>Přidat kategoriu</p>
        <div className='modalThread'>
          <form onSubmit={handleSubmit}>
              <div>
                  <TextField variant="outlined" fullWidth type="text" label="Názov kategórie" margin="normal"
                  name="category" value={categoryName} onChange={handleCategoryChange} required/>
              </div>
              <div id="subButDiv">
                  <Button type="submit" variant="contained">Přidat</Button>
              </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default AddCategory
