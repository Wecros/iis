import React, {useState, useEffect, useContext} from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import { Typography } from '@mui/material';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import {AnswerCard} from '../cards'
import { UserContext } from '../../providers/UserContext'
import './Thread.css'

const Thread = ({threadId, setThreadId, message}) => {
  const {token, sendRequest} = useContext(UserContext)
  const [threadMessages, setThreadMessages] = useState({})

  const fetchThread = () => {
    const body = {
    }
    const response = sendRequest('GET', 'http://localhost:8000/api/answers/' + threadId + '/thread', JSON.stringify(body))
    response.then(res => {
      if(res === null)
      {
        const response = sendRequest('POST', 'http://localhost:8000/api/answers/' + threadId + '/thread', JSON.stringify(body))
        response.then(res => {
          setThreadMessages(res)
        })
      }
      else
      {
        setThreadMessages(res)
      }
    })
  }
  const [messageText, setMessageText] = useState('')

  const handleMessageText = (e) =>
  {
    setMessageText(e.target.value)
  }

  const handleSendMessage = () =>
  {
    const body = {
      text: messageText,
    }
    const response = sendRequest('POST', 'http://localhost:8000/api/threads/' + threadMessages.id + '/thread_answers', JSON.stringify(body))
    response.then(res => {
      setMessageText('')
      fetchThread()
    })
  }

  useEffect(()=> {
    fetchThread()
  }, [sendRequest, token])
  return (
    <div id="myModal" className="modal" style={threadId === -1 ? {display:'none'}: {display:'block'}}>
      <div className="modal-content">
        <span className="close" onClick={()=>setThreadId(-1)}>&times;</span>
        <p>Vlákno</p>
        <div className='modalThread'>
          <AnswerCard data={message} />
          {threadMessages?.answers ? threadMessages.answers.map((data) =>(
            <AnswerCard data={data} />
          ))
            : ''
          }
          <div className='newQuestion'>
            <Card>
              <CardContent>
                <div className='questionFlex'>
                  <Typography>Zadejte odpověď</Typography>
                  <TextareaAutosize
                    aria-label="minimum height"
                    minRows={4}
                    style={{ width: '100%' }}
                    value={messageText}
                    onChange={handleMessageText}
                  />
                </div>
                <Button variant="contained" onClick={handleSendMessage}>Odeslat</Button>
              </CardContent>
            </Card>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Thread
