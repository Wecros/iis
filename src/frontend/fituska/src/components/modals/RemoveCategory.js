import React, {useState, useContext, useEffect} from 'react'
import {Button, FormControl, InputLabel, Select, MenuItem} from '@mui/material';
import { UserContext } from '../../providers/UserContext'
import './RemoveCategory.css'

const RemoveCategory = ({isRemoveCategory, setRemoveCategory, id}) => {
  const [categoryName, setCategoryName] = useState('')
  const {sendRequest} = useContext(UserContext)

  const [filterCategory, setFilterCategory] = useState('')
  const [categories, setCategories] = useState([])

  useEffect(()=> {
    const data = sendRequest('GET', 'http://localhost:8000/api/courses/' + id + '/categories', {})
    data.then(res => {
      setCategories(res)
    })
  }, [id])

  const handleFilter = (e) => {
    setFilterCategory(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const data = sendRequest('DELETE', 'http://localhost:8000/api/categories/' + filterCategory , JSON.stringify({name: categoryName}))
    data.then(res => console.log(res))
    setRemoveCategory(false)
  }

  return (
    <div id="myModal" className="modal" style={isRemoveCategory === false  ? {display:'none'}: {display:'block'}}>
      <div className="modal-content">
        <span className="close" onClick={()=>setRemoveCategory(false)}>&times;</span>
        <p>Odstranit kategoriu</p>
        <div className='modalThread'>
          <form onSubmit={handleSubmit}>
              <FormControl variant="standard" sx={{ m: 1, minWidth: '120px' }}>
                <InputLabel id="demo-simple-select-standard-label">Kategórie</InputLabel>
                <Select
                  labelId="demo-simple-select-standard-label"
                  id="demo-simple-select-standard"
                  value={filterCategory}
                  onChange={handleFilter}
                  label="Age"
                  required
                >
                  {categories.map((data, idx) => (
                    <MenuItem value={data.id} key={idx}>{data.name}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <div id="subButDiv">
                  <Button type="submit" variant="contained" sx={{backgroundColor: 'red'}}>Odstranit</Button>
              </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default RemoveCategory
