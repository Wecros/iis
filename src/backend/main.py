#!/usr/bin/env python3
import uvicorn

if __name__ == "__main__":
    uvicorn.run("fituska.api:app", port=8000, reload=True)
