from datetime import timedelta
from operator import attrgetter
from typing import Any, List

from alembic.command import current
from fastapi import APIRouter, Depends, FastAPI, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import OAuth2PasswordRequestForm
from fituska import crud, models, schemas
from fituska.database import engine
from fituska.dependencies import (
    db_init,
    get_current_admin,
    get_current_moderator,
    get_current_user,
    get_db,
)
from fituska.security import (
    ACCESS_TOKEN_EXPIRE_MINUTES,
    authenticate_user,
    create_access_token,
    oauth2_scheme,
)
from sqlalchemy.orm import Session

app = FastAPI(
    title="Fituška 2.0",
    docs_url="/api/docs",
    redoc_url=None,
    openapi_url="/api/openapi.json",
)

# Allow cross-origin requests from our frontend running at port 3000.
origins = ["http://localhost:3000", "localhost:3000"]

# Allow credentials, any HTTP method and any HTTP request headers.
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Create all of the DB tables, bound to the running DB server.
models.Base.metadata.create_all(bind=engine)

db_init()

# Prefix all of our requests with /api
base_router = APIRouter(prefix="/api")


@base_router.get("/")
def root():
    """Endpoint to list all of the other possible endpoints.

    Authorization: Not required.
    """
    url_list = [{"path": route.path, "name": route.name} for route in app.routes]
    return url_list


@base_router.post("/token", response_model=schemas.Token)
async def login_for_access_token(
    db: Session = Depends(get_db), form_data: OAuth2PasswordRequestForm = Depends()
):
    """Endpoint for logging in, call this token when needed to log in.

    Raises 401:
        - Incorrect username or password.
    Raises 410:
        - User account has been deactivated.

    Returns: access_token that is supplied in header when calling other endpoints.

    Authorization: Not required.
    """
    user = authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    if not user.is_active:
        raise HTTPException(
            status_code=status.HTTP_410_GONE,
            detail="The user's account has been deactivated.",
        )

    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@base_router.get("/me", response_model=schemas.User)
def read_me(
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for returning information about currently logged in user.

    Authorization: Logged in user.
    """
    return current_user


@base_router.put("/me")
def modify_me(
    user: schemas.UserBase,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for modifying user account of the current logged in user.

    Authorization: Logged in user.
    """
    return crud.modify_user(db=db, user=user, user_id=current_user.id)


@base_router.delete("/me")
def delete_me(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for deleting user account of the current logged in user.

    Authorization: Logged in user.
    """
    return crud.delete_user(db=db, user_id=current_user.id)


@base_router.get(
    "/me/courses/teach", response_model=List[schemas.CoursePublicForEndpoint]
)
def read_my_taught_courses(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for listing courses the the current logged in user teaches.

    Authorization: Logged in user.
    """
    my_courses = (
        db.query(models.Course)
        .filter(models.Course.teacher_id == current_user.id)
        .all()
    )
    new_courses = get_public_courses_scheme(db, my_courses)
    return new_courses


@base_router.get(
    "/me/courses/registered", response_model=List[schemas.CoursePublicForEndpoint]
)
def read_my_registered_courses(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for listing courses the the current logged in user is registered to,
    the registration was approved by the teacher.

    Authorization: Logged in user.
    """
    registered_courses_rels = (
        db.query(models.students_courses)
        .filter(
            models.students_courses.c.student_id == current_user.id,
            models.students_courses.c.approved == True,
        )
        .all()
    )
    rcr_ids = []
    for rcr in registered_courses_rels:
        rcr_ids.append(rcr["course_id"])
    my_courses = (
        db.query(models.Course)
        .filter(
            models.Course.students.any(models.User.id == current_user.id),
            models.Course.id.in_(rcr_ids),
        )
        .all()
    )

    new_courses = get_public_courses_scheme(db, my_courses)
    return new_courses


@base_router.get(
    "/me/courses/pending_registration", response_model=List[schemas.CoursePublic]
)
def read_my_pending_registration_courses(
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for listing courses the the current logged in user is registered to,
    but is waiting for the approval of the teacher.

    Authorization: Logged in user.
    """
    pending_courses_rels = (
        db.query(models.students_courses)
        .filter(
            models.students_courses.c.student_id == current_user.id,
            models.students_courses.c.approved == False,
        )
        .all()
    )
    pcr_ids = []
    for pcr in pending_courses_rels:
        pcr_ids.append(pcr["course_id"])
    pending_courses = (
        db.query(models.Course)
        .filter(
            models.Course.students.any(models.User.id == current_user.id),
            models.Course.id.in_(pcr_ids),
        )
        .all()
    )
    return pending_courses


@base_router.post("/me/courses/", response_model=schemas.CoursePrivate)
def create_course_for_teacher(
    course: schemas.CourseCreate,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for creating course with teacher as the current logged in user.

    Raises 400:
        - Course abbreviation already defined before.

    Authorization: Logged in user.
    """
    existing_course = db.query(models.Course).filter(models.Course.abbreviation == course.abbreviation).first()
    if existing_course:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Course with given abbreviation already exists",
        )

    return crud.create_teacher_course(db=db, course=course, teacher_id=current_user.id)


@base_router.get("/users/public", response_model=List[schemas.User])
def read_active_users(
    db: Session = Depends(get_db),
):
    """Endpoint that lists all of the active users of the database.

    Authorization: Not required.
    """
    users = db.query(models.User).filter(models.User.is_active == True).all()
    return users


@base_router.get(
    "/users/private",
    response_model=List[schemas.User],
    dependencies=[Depends(get_current_admin)],
)
def read_all_users(
    db: Session = Depends(get_db),
):
    """Endpoint that lists all of the users (even inactive) of the database.

    Authorization: Admin.
    """
    users = db.query(models.User).all()
    return users


@base_router.post("/users/", response_model=schemas.User)
def create_user(
    user: schemas.UserCreate,
    db: Session = Depends(get_db),
):
    """Endpoint for creating new user. Used during registration or on admin page.

    Authorization: Not required.
    """
    username = crud.get_user_by_username(db, user.username)
    email = crud.get_user_by_email(db, user.email)
    if username or email:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User with given username or email already exists",
        )

    return crud.create_user(db=db, user=user)


@base_router.get(
    "/users/{user_id}",
    response_model=schemas.User,
    dependencies=[Depends(get_current_moderator)],
)
def read_user(user_id: int, db: Session = Depends(get_db)):
    """Endpoint for getting information about specific user from the database.

    Raises 404:
        - Nonexistent user.

    Authorization: Moderator.
    """
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User with that ID not found."
        )
    return db_user


@base_router.put("/users/{user_id}", dependencies=[Depends(get_current_admin)])
def modify_user(
    user_id: int,
    user: schemas.UserUpdate,
    db: Session = Depends(get_db),
):
    """Endpoint for changing information about a specific user from the database.

    Raises 404:
        - Nonexistent user.

    Returns: Number of modified users (should be 1).

    Authorization: Admin.
    """
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User with that ID not found."
        )
    return crud.modify_user(db=db, user=user, user_id=user_id)


@base_router.delete("/users/{user_id}", dependencies=[Depends(get_current_admin)])
def delete_user(
    user_id: int,
    db: Session = Depends(get_db),
):
    """Endpoint for deleting (deactivating) a specific user from the database.

    Raises 404:
        - Nonexistent user.

    Returns: Number of deleted users (should be 1).

    Authorization: Admin.
    """
    db_user = crud.get_user(db, user_id=user_id)
    if db_user is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="User with that ID not found."
        )
    return crud.delete_user(db=db, user_id=user_id)


@base_router.get(
    "/courses/public", response_model=List[schemas.CoursePublicForEndpoint]
)
def read_public_courses(db: Session = Depends(get_db)):
    """Endpoint for returning courses that have been approved from the moderator.

    Returns:
        - Course object
        - registered_count: Number of registered users to the specified course.
        - questions_count: Number of questions asked in the specified course.

    Authorization: Not required.
    """
    courses = db.query(models.Course).filter(models.Course.approved == True).all()
    new_courses = get_public_courses_scheme(db, courses)
    return new_courses


def get_public_courses_scheme(db: Session, courses):
    new_courses = []
    for course in courses:
        # Start of Stack Overflow content
        # Source: https://stackoverflow.com/questions/7300948/add-column-to-sqlalchemy-table
        # Answer's authors:
        #   - original: Demian Brecht (https://stackoverflow.com/users/534476/demian-brecht)
        #   - edited: James Mishra (https://stackoverflow.com/users/49143/james-mishra)
        # Question author:
        #   - original: Chris (https://stackoverflow.com/users/765357/chris)
        #   - edited: Phillip (https://stackoverflow.com/users/4907496/phillip)
        row2dict = lambda r: {
            c.name: str(getattr(r, c.name)) for c in r.__table__.columns
        }
        # End of Stack Overflow content
        new_course = {
            **row2dict(course),
            "teacher": {**row2dict(course.teacher)},
            "categories": [{**row2dict(category)} for category in course.categories],
        }

        approved_students_count = 0
        for student in course.students:
            if (
                db.query(models.students_courses)
                .filter(models.students_courses.c.student_id == student.id)
                .first()
                .approved
            ):
                approved_students_count += 1
        new_course["registered_count"] = approved_students_count

        question_count = 0
        for category in course.categories:
            question_count += len(category.questions)
        new_course["question_count"] = question_count
        new_courses.append(new_course)
    return new_courses


@base_router.get(
    "/courses/private",
    response_model=List[schemas.CoursePrivate],
    dependencies=[Depends(get_current_moderator)],
)
def read_private_courses(db: Session = Depends(get_db)):
    """Endpoint for returning both approved and unapproved courses.

    Authorization: Moderator.
    """
    return db.query(models.Course).all()


@base_router.put(
    "/courses/approve/{course_id}",
    dependencies=[Depends(get_current_moderator)],
)
def approve_course(course_id: int, db: Session = Depends(get_db)):
    """Endpoint for approving created courses by the teachers.

    raises 404:
        - Nonexistent course.

    Authorization: Moderator.
    """
    course = db.query(models.Course).get({"id": course_id})
    if not course:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Course with that ID does not exist.",
        )

    rows_changed = (
        db.query(models.Course)
        .filter(models.Course.id == course_id)
        .update({models.Course.approved: True})
    )
    if rows_changed > 0:
        db.commit()
    return rows_changed


@base_router.post("/courses/register/{course_id}", response_model=schemas.CoursePrivate)
def register_to_course(
    course_id: int,
    db: Session = Depends(get_db),
    current_user=Depends(get_current_user),
):
    """Endpoint for registering current user to the course.

    raises 404:
        - Nonexistent course.

    raises 400:
        - User already registered.

    Authorization: Logged in user.
    """
    course = db.query(models.Course).get({"id": course_id})
    if not course:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Course with that ID does not exist.",
        )

    if is_user_registered_to_course(db, current_user, course_id):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="You are already registered to this course.",
        )

    course = db.query(models.Course).get({"id": course_id})
    course.students.append(current_user)
    db.commit()
    db.refresh(course)
    return course


@base_router.delete(
    "/courses/{course_id}", dependencies=[Depends(get_current_moderator)]
)
def delete_course(
    course_id: int,
    db: Session = Depends(get_db),
):
    """Endpoint for deleting/disapproving courses.

    raises 404:
        - Nonexistent course.

    Authorization: Moderator.
    """
    course = db.query(models.Course).get({"id": course_id})
    if not course:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Course with that ID does not exist.",
        )

    deleted_rows = (
        db.query(models.Course).filter(models.Course.id == course_id).delete()
    )
    if deleted_rows > 0:
        db.commit()
    return deleted_rows


@base_router.get(
    "/courses/{course_id}/pending_registrations",
    response_model=List[schemas.UserBaseWithID],
)
def read_pending_registrations_my_course(
    course_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for returning list of all students waiting for registration confirmation for specific course.

    Raises 404:
        - Nonexistent Course.

    Raises 403:
        - Current user is not teacher, moderator or admin.

    Authorization: Teacher.
    """
    course = db.query(models.Course).get({"id": course_id})
    if not course:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Course with that ID does not exist.",
        )
    if (
        current_user.role not in [models.UserType.admin, models.UserType.moderator]
        and course.teacher_id != current_user.id
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Insufficient permissions."
        )

    student_rels = (
        db.query(models.students_courses)
        .filter(
            models.students_courses.c.course_id == course_id,
            models.students_courses.c.approved == False,
        )
        .all()
    )
    student_ids = []
    for sr in student_rels:
        student_ids.append(sr["student_id"])
    return db.query(models.User).filter(models.User.id.in_(student_ids)).all()


@base_router.put("/courses/{course_id}/registration/{student_id}/approve")
def confirm_user_course_registration(
    course_id: int,
    student_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for confirming specific user registration for specific course.

    Raises 404:
        - Nonexistent course or nonexistent student.

    Raises 403:
        - Current user is not teacher, moderator or admin.

    Authorization: Teacher.
    """
    course = db.query(models.Course).get({"id": course_id})
    if not course:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Course with that ID does not exist.",
        )

    student = db.query(models.User).get({"id": student_id})
    if not student:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Student with that ID does not exist.",
        )

    if (
        current_user.role not in [models.UserType.admin, models.UserType.moderator]
        and course.teacher_id != current_user.id
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Insufficient permissions."
        )

    rows_changed = (
        db.query(models.students_courses)
        .filter(
            models.students_courses.c.course_id == course_id,
            models.students_courses.c.student_id == student_id,
            models.students_courses.c.approved == False,
        )
        .update({models.students_courses.c.approved: True})
    )
    if rows_changed > 0:
        db.commit()
    return rows_changed


@base_router.put(
    "/courses/{course_id}/registration/{student_id}/disapprove",
)
def disapprove_user_course_registration(
    course_id: int,
    student_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for rejecting user registration for specific course.

    Raises 404:
        - Nonexistent course or nonexistent student.

    Raises 403:
        - Current user is not teacher, moderator or admin.

    Authorization: Moderator.
    """
    course = db.query(models.Course).get({"id": course_id})
    if not course:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Course with that ID does not exist.",
        )

    student = db.query(models.User).get({"id": student_id})
    if not student:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Student with that ID does not exist.",
        )

    if (
        current_user.role not in [models.UserType.admin, models.UserType.moderator]
        and course.teacher_id != current_user.id
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Insufficient permissions."
        )

    rows_deleted = (
        db.query(models.students_courses)
        .filter(
            models.students_courses.c.course_id == course_id,
            models.students_courses.c.student_id == student_id,
            models.students_courses.c.approved == False,
        )
        .delete()
    )
    if rows_deleted > 0:
        db.commit()
    return rows_deleted


@base_router.get(
    "/courses/{course_id}/categories", response_model=List[schemas.Category]
)
def read_categories(
    course_id: int,
    db: Session = Depends(get_db),
):
    """Endpoint for returning all categories of a course.

    Raises 404:
        - Nonexistent course.

    """
    course = db.query(models.Course).get({"id": course_id})
    if not course:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Course with that ID does not exist.",
        )

    return (
        db.query(models.Category).filter(models.Category.course_id == course_id).all()
    )


@base_router.post("/courses/{course_id}/categories", response_model=schemas.Category)
def create_category(
    category: schemas.CategoryCreate,
    course_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for creating new course category.

    Raises 404:
        - Nonexistent course.

    Raises 403:
        - Logged in user is not teacher of the course, moderator or admin.

    Authorization: Teacher.
    """
    course = db.query(models.Course).get({"id": course_id})
    if not course:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Course with that ID does not exist.",
        )

    if (
        current_user.role not in [models.UserType.admin, models.UserType.moderator]
        and course.teacher_id != current_user.id
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Insufficient permissions."
        )

    db_category = models.Category(**category.dict(), course_id=course_id)
    db.add(db_category)
    db.commit()
    db.refresh(db_category)
    return db_category


@base_router.delete("/categories/{category_id}")
def delete_category(
    category_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for deleting specific category of a course.

    Raises 404:
        - Nonexistent category.

    Raises 403:
        - Logged in user is not the teacher of the course, moderator or admin.

    Authorization: Admin.
    """
    # TODO
    category = db.query(models.Category).get({"id": category_id})
    if not category:
        raise HTTPException(
            status_code=status.HTTP_404_BAD_REQUEST,
            detail="Category with that ID does not exist.",
        )

    course = db.query(models.Course).get({"id": category.course_id})
    if (
        current_user.role not in [models.UserType.admin, models.UserType.moderator]
        and course.teacher_id != current_user.id
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Insufficient permissions."
        )

    deleted_rows = (
        db.query(models.Category).filter(models.Category.id == category_id).delete()
    )
    if deleted_rows > 0:
        db.commit()
    return deleted_rows


@base_router.get("/course/{course_id}/questions", response_model=List[schemas.Question])
def read_all_questions(
    course_id: int,
    db: Session = Depends(get_db),
):
    """Endpoint for returning all questions belonging to a course.

    Authorization: Not required.
    """
    category_ids = (
        db.query(models.Category.id)
        .filter(models.Category.course_id == course_id)
        .all()
    )
    category_ids = [ci for (ci,) in category_ids]
    return (
        db.query(models.Question)
        .filter(models.Question.category_id.in_(category_ids))
        .all()
    )


@base_router.get(
    "/categories/{category_id}/questions", response_model=List[schemas.Question]
)
def read_questions(
    category_id: int,
    db: Session = Depends(get_db),
):
    """Endpoint for returning all of questions of specific category.

    Raises 404:
        - Nonexistent category.

    Authorization: Not required.
    """
    category = db.query(models.Category).get({"id": category_id})
    if not category:
        raise HTTPException(
            status_code=status.HTTP_404_BAD_REQUEST,
            detail="Category with that ID does not exist.",
        )

    return (
        db.query(models.Question)
        .filter(models.Question.category_id == category_id)
        .all()
    )


@base_router.post("/categories/{category_id}/questions")
def create_question(
    category_id: int,
    question: schemas.QuestionCreate,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for creating questions.

    Raises 404:
        - Nonexistent category.

    Raises 403:
        - User is not registered in the course or is not moderator or is not a teacher

    Authorization: Logged in user.
    """
    category = db.query(models.Category).get({"id": category_id})
    if not category:
        raise HTTPException(
            status_code=status.HTTP_404_BAD_REQUEST,
            detail="Category with that ID does not exist.",
        )

    course = db.query(models.Course).get({"id": category.course_id})
    if (
        not is_user_approved_to_course(db, current_user, category.course_id)
        and current_user.role not in [models.UserType.admin, models.UserType.moderator]
        and current_user.id != course.teacher_id
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Unapproved students of the course cannot create questions.",
        )

    db_question = models.Question(
        **question.dict(),
        category_id=category_id,
        author_id=current_user.id,
        is_checked=False
    )
    db.add(db_question)
    db.commit()
    db.refresh(db_question)
    return db_question


@base_router.delete(
    "/questions/{question_id}", dependencies=[Depends(get_current_admin)]
)
def delete_question(
    question_id: int,
    db: Session = Depends(get_db),
):
    """Endpoint for deleting questions.

    Raises 404:
        - Nonexistent question.

    Authorization: Admin.
    """
    question = db.query(models.Question).get({"id": question_id})
    if not question:
        raise HTTPException(
            status_code=status.HTTP_404_BAD_REQUEST,
            detail="Question with that ID does not exist.",
        )

    deleted_rows = (
        db.query(models.Question).filter(models.Question.id == question_id).delete()
    )
    if deleted_rows > 0:
        db.commit()
    return deleted_rows


@base_router.put("/questions/{question_id}")
def check_question(
    question_id: int,
    answer: schemas.AnswerCreate,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for marking the question as checked.

    Raises 404:
        - Nonexistent question.

    Raises 403:
        - Logged in user is not the teacher of the course, moderator or admin.

    Raises 400:
        - Question is already checked.

    Authorization: Teacher of the course.
    """
    question = db.query(models.Question).get({"id": question_id})
    if not question:
        raise HTTPException(
            status_code=status.HTTP_404_BAD_REQUEST,
            detail="Question with that ID does not exist.",
        )
    if question.is_checked:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Question was already checked.",
        )

    category = db.query(models.Category).get({"id": question.category_id})
    course = db.query(models.Course).get({"id": category.course_id})

    if (
        current_user.role not in [models.UserType.admin, models.UserType.moderator]
        and course.teacher_id != current_user.id
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Insufficient permissions."
        )

    db_answer = models.Answer(
        **answer.dict(), question_id=question_id, author_id=current_user.id
    )
    db_answer.is_correct = True
    db.add(db_answer)
    db.commit()
    db.refresh(db_answer)

    rows_changed = (
        db.query(models.Question)
        .filter(models.Question.id == question_id)
        .update({models.Question.is_checked: True})
    )
    if rows_changed > 0:
        db.commit()
    return rows_changed


@base_router.get(
    "/questions/{question_id}/answers", response_model=List[schemas.Answer]
)
def read_answers(
    question_id: int,
    db: Session = Depends(get_db),
):
    """Endpoint for getting answers of a specific question.

    Raises 404:
        - Nonexistent question.

    Returns: List of answers.

    Authorization: Not required.
    """
    question = db.query(models.Question).get({"id": question_id})
    if not question:
        raise HTTPException(
            status_code=status.HTTP_404_BAD_REQUEST,
            detail="Question with that ID does not exist.",
        )

    return (
        db.query(models.Answer).filter(models.Answer.question_id == question_id).all()
    )


@base_router.post("/questions/{question_id}/answers")
def create_answer(
    answer: schemas.AnswerCreate,
    question_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for creating answer to specific question.

    Raises 404:
        - Nonexistent answer.

    Raises 403:
        - User tried to answer his own question.
        - User is not registered in the course or is not moderator

    Raises 400:
        - Currently logged in user has already submitted answer to the question.

    Authorization: Logged in user.
    """
    question = db.query(models.Question).get({"id": question_id})
    if not question:
        raise HTTPException(
            status_code=status.HTTP_404_BAD_REQUEST,
            detail="Question with that ID does not exist.",
        )

    if question.author_id == current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You cannot answer your own question.",
        )

    category = db.query(models.Category).get({"id": question.category_id})
    if not is_user_approved_to_course(
        db, current_user, category.course_id
    ) and current_user.role not in [models.UserType.admin, models.UserType.moderator]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Unapproved students of the course cannot create answers.",
        )

    existing_answer = (
        db.query(models.Answer)
        .filter(
            models.Answer.author_id == current_user.id,
            models.Answer.question_id == question_id,
        )
        .first()
    )

    if existing_answer:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="You have already submited answer to this question",
        )
    db_answer = models.Answer(
        **answer.dict(), question_id=question_id, author_id=current_user.id
    )
    db.add(db_answer)
    db.commit()
    db.refresh(db_answer)
    return db_answer


@base_router.put("/answers/{answer_id}")
def evaluate_correct_answer(
    answer_id: int,
    extra_votes: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for evaluating correct answers. Count of total answer's reactions + extra_votes
    parameter are added to the author's karma.

    Raises 404:
        - Nonexistent answer.

    Raises 403:
        - Logged in user is not teacher of the course, moderator or admin.

    Authorization: Needs to be teacher of the course.
    """
    if extra_votes < 0:
        extra_votes = 0

    answer = db.query(models.Answer).get({"id": answer_id})
    if not answer:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Answer with that ID does not exist.",
        )
    question = db.query(models.Question).get({"id": answer.question_id})
    category = db.query(models.Category).get({"id": question.category_id})
    course = db.query(models.Course).get({"id": category.course_id})

    if (
        current_user.role not in [models.UserType.admin, models.UserType.moderator]
        and course.teacher_id != current_user.id
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Insufficient permissions."
        )

    answers_changed = (
        db.query(models.Answer)
        .filter(models.Answer.id == answer.id)
        .update({models.Answer.is_correct: True})
    )

    for extra_vote in range(extra_votes):
        db_reaction = models.Reaction(answer_id=answer_id, author_id=current_user.id)
        db.add(db_reaction)

    users_changed = (
        db.query(models.User)
        .filter(models.User.id == answer.author.id)
        .update(
            {models.User.karma: models.User.karma + len(answer.reactions) + extra_votes}
        )
    )

    if answers_changed > 0:
        db.commit()
    return answers_changed


@base_router.get("/answers/{answer_id}/thread", response_model=schemas.Thread)
def get_thread(
    answer_id: int,
    db: Session = Depends(get_db),
):
    """Endpoint for returning thread of a specific answer.

    Raises 404:
        - Nonexistent answer.

    Authorization: Not required.
    """
    answer = db.query(models.Answer).get({"id": answer_id})
    if not answer:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Answer with that ID does not exist.",
        )

    return db.query(models.Thread).get({"id": answer_id})


@base_router.post("/answers/{answer_id}/thread", response_model=schemas.Thread)
def create_thread(
    thread: schemas.ThreadCreate,
    answer_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for creating thread of a specific answer.

    Raises 404:
        - Nonexistent answer.

    Raises 403:
        - User is not registered in the course or is not moderator

    Authorization: Logged in user.
    """
    answer = db.query(models.Answer).get({"id": answer_id})
    if not answer:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Answer with that ID does not exist.",
        )

    question = db.query(models.Question).get({"id": answer.question_id})
    category = db.query(models.Category).get({"id": question.category_id})
    if not is_user_approved_to_course(
        db, current_user, category.course_id
    ) and current_user.role not in [models.UserType.admin, models.UserType.moderator]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Unapproved students of the course cannot create threads.",
        )

    db_thread = models.Thread(**thread.dict(), original_answer_id=answer_id)
    db.add(db_thread)
    db.commit()
    db.refresh(db_thread)
    return db_thread


@base_router.get(
    "/threads/{thread_id}/thread_answers", response_model=List[schemas.ThreadAnswer]
)
def read_thread_answers(
    thread_id: int,
    db: Session = Depends(get_db),
):
    """Endpoint for returning all of the thread answers of particular thread.

    Raises 404:
        - Nonexistent thread.

    Authorization: Not required.
    """
    thread = db.query(models.Thread).get({"id": thread_id})
    if not thread:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Thread with that ID does not exist.",
        )
    thread_answers = (
        db.query(models.ThreadAnswer)
        .filter(models.ThreadAnswer.thread_id == thread_id)
        .all()
    )
    return thread_answers


@base_router.post(
    "/threads/{thread_id}/thread_answers", response_model=schemas.ThreadAnswer
)
def create_thread_answer(
    thread_answer: schemas.ThreadAnswerCreate,
    thread_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for creating thread answers.

    Raises 404:
        - Nonexistent thread.

    Raises 403:
        - User is not registered in the course or is not moderator

    Authorization: Logged in user.
    """
    db_thread = db.query(models.Thread).get({"id": thread_id})
    if not db_thread:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Thread with that ID does not exist.",
        )

    answer = db.query(models.Answer).get({"id": db_thread.original_answer_id})
    question = db.query(models.Question).get({"id": answer.question_id})
    category = db.query(models.Category).get({"id": question.category_id})
    if not is_user_approved_to_course(
        db, current_user, category.course_id
    ) and current_user.role not in [models.UserType.admin, models.UserType.moderator]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Unapproved students of the course cannot create thread answers.",
        )

    db_thread_answer = models.ThreadAnswer(
        **thread_answer.dict(), thread_id=thread_id, author_id=current_user.id
    )
    db.add(db_thread_answer)
    db.commit()
    db.refresh(db_thread_answer)
    return db_thread_answer


@base_router.post("/answers/{answer_id}/reactions", response_model=schemas.Reaction)
def react_to_answer(
    answer_id: int,
    reaction: schemas.ReactionCreate,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for giving reaction to an answer.

    Raises 404:
        - Nonexistent answer.

    Raises 403:
        - User is not registered in the course or is not moderator

    Raises 400:
        - When there are more than 3 reactions per question for logged in user.
        - When logged in user already reacted to the answer.

    Authorization: Logged in user.
    """
    reactions_per_question = 0
    answer = db.query(models.Answer).get({"id": answer_id})
    if not answer:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="No answer of that ID found."
        )

    question = db.query(models.Question).get({"id": answer.question_id})
    category = db.query(models.Category).get({"id": question.category_id})
    if not is_user_approved_to_course(
        db, current_user, category.course_id
    ) and current_user.role not in [models.UserType.admin, models.UserType.moderator]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Unapproved students of the course cannot create reactions.",
        )

    question = db.query(models.Question).get({"id": answer.question.id})
    if question.is_checked or answer.is_correct:
        print("here")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="You can't add reactions to already checked questions and answers.",
        )

    reactions_per_question = (
        db.query(models.Reaction)
        .join(models.Answer)
        .filter(
            models.Reaction.author_id == current_user.id,
            models.Answer.question_id == question.id,
        )
        .count()
    )
    reactions_per_answer = (
        db.query(models.Reaction)
        .filter(
            models.Reaction.answer_id == answer.id,
            models.Reaction.author_id == current_user.id,
        )
        .count()
    )

    if reactions_per_question >= 3:
        print("hos")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="You can only react to 3 answers per question.",
        )
    if reactions_per_answer >= 1:
        print("hey")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="You can only add 1 reaction per answer.",
        )

    db_reaction = models.Reaction(
        **reaction.dict(), answer_id=answer_id, author_id=current_user.id
    )
    db.add(db_reaction)
    db.commit()
    db.refresh(db_reaction)
    return db_reaction


@base_router.delete("/reactions/{reaction_id}")
def delete_reaction(
    reaction_id: int,
    db: Session = Depends(get_db),
    current_user: schemas.User = Depends(get_current_user),
):
    """Endpoint for deleting reactions of currently logged in user.

    Raises 404:
        - Reaction does not exist.

    Raises 403:
        - Deleted reaction is not that of currently logged in user.
    """
    reaction = db.query(models.Reaction).get({"id": reaction_id})
    if not reaction:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Reaction does not exist."
        )
    if reaction.author.id != current_user.id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You cannot change other users' reactions.",
        )

    deleted_rows = (
        db.query(models.Reaction).filter(models.Reaction.id == reaction_id).delete()
    )
    if deleted_rows > 0:
        db.commit()
    return deleted_rows


@base_router.get("/fame", response_model=schemas.HallOfFame)
def read_hall_of_fame(db: Session = Depends(get_db)):
    """Endpoint for listing the top users with most votes (karma).

    Returns: {
        "all_courses": top 10 list of users with their karma,
        "individiual_courses": lists of courses with lists of top 10 users in them
    }

    Authorization: Not required.
    """
    top_all_courses = (
        db.query(models.User).order_by(models.User.karma.desc()).limit(10).all()
    )
    courses = db.query(models.Course).filter(models.Course.approved == True).all()

    individual_courses_list = []
    for course in courses:
        top_individual_course = get_top10_from_course(db, course)
        individual_courses_list.append(
            {
                "course": {"abbreviation": course.abbreviation, "id": course.id},
                "top_users": top_individual_course,
            }
        )

    return {
        "all_courses": top_all_courses,
        "individual_courses": individual_courses_list,
    }


def get_top10_from_course(db: Session, course: models.Course):
    user_karma_map = {}
    for category in course.categories:
        for question in category.questions:
            for answer in question.answers:
                if answer.is_correct:
                    for reaction in answer.reactions:
                        user_karma_map.setdefault(answer.author_id, 0)
                        user_karma_map[answer.author_id] += 1

    user_list = []
    for user_id, karma in user_karma_map.items():
        db_user = db.query(models.User).get({"id": user_id})
        user_list.append(
            {
                "username": db_user.username,
                "name": db_user.name,
                "surname": db_user.surname,
                "karma": karma,
            }
        )
    top_list = sorted(user_list, key=lambda d: d["karma"], reverse=True)[:10]

    return top_list


def is_user_registered_to_course(db: Session, current_user, course_id):
    student_rels = (
        db.query(models.students_courses)
        .filter(
            models.students_courses.c.course_id == course_id,
        )
        .all()
    )
    student_ids = []
    for sr in student_rels:
        student_ids.append(sr["student_id"])
    if current_user.id in student_ids:
        return True
    return False


def is_user_approved_to_course(db: Session, current_user, course_id):
    student_rels = (
        db.query(models.students_courses)
        .filter(
            models.students_courses.c.course_id == course_id,
            models.students_courses.c.approved == True,
        )
        .all()
    )
    student_ids = []
    for sr in student_rels:
        student_ids.append(sr["student_id"])
    if current_user.id in student_ids:
        return True
    return False


app.include_router(base_router)
