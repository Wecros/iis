import enum

from fituska.database import Base
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Enum,
    ForeignKey,
    Integer,
    String,
    Table,
)
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func


class UserType(enum.Enum):
    admin = "admin"
    moderator = "moderator"
    standard = "standard"


students_courses = Table(
    "students_courses",
    Base.metadata,
    Column("student_id", ForeignKey("users.id"), primary_key=True),
    Column("course_id", ForeignKey("courses.id"), primary_key=True),
    Column("approved", Boolean, default=False),
)


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_onupdate=func.now())
    is_active = Column(Boolean, default=True)

    hashed_password = Column(String, nullable=False)
    username = Column(String, unique=True, nullable=False)
    email = Column(String, unique=True, nullable=False)
    name = Column(String, nullable=False)
    surname = Column(String, nullable=False)
    karma = Column(Integer, default=0, nullable=False)
    role = Column(Enum(UserType), default=UserType.standard, nullable=False)

    taught_courses = relationship("Course", back_populates="teacher")
    registered_courses = relationship(
        "Course", secondary=students_courses, back_populates="students"
    )
    questions = relationship("Question", back_populates="author")
    answers = relationship("Answer", back_populates="author")
    reactions = relationship("Reaction", back_populates="author")
    thread_answers = relationship("ThreadAnswer", back_populates="author")


class Course(Base):
    __tablename__ = "courses"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_onupdate=func.now())

    name = Column(String)
    abbreviation = Column(String, unique=True)
    description = Column(String)
    approved = Column(Boolean, default=False)

    teacher_id = Column(Integer, ForeignKey("users.id"))
    teacher = relationship("User", back_populates="taught_courses")
    students = relationship(
        "User", secondary=students_courses, back_populates="registered_courses"
    )
    categories = relationship("Category", back_populates="course")


class Category(Base):
    __tablename__ = "categories"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_onupdate=func.now())

    name = Column(String)
    course_id = Column(Integer, ForeignKey("courses.id"))
    course = relationship("Course", back_populates="categories")
    questions = relationship(
        "Question", back_populates="category", passive_deletes=True
    )


class Question(Base):
    __tablename__ = "questions"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_onupdate=func.now())

    name = Column(String)
    details = Column(String)
    is_checked = Column(Boolean, default=False)

    category_id = Column(Integer, ForeignKey("categories.id", ondelete="CASCADE"))
    author_id = Column(Integer, ForeignKey("users.id"))
    category = relationship("Category", back_populates="questions")
    author = relationship("User", back_populates="questions")
    answers = relationship("Answer", back_populates="question", passive_deletes=True)


class Answer(Base):
    __tablename__ = "answers"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_onupdate=func.now())

    text = Column(String)
    is_correct = Column(Boolean, default=False)

    question_id = Column(Integer, ForeignKey("questions.id", ondelete="CASCADE"))
    author_id = Column(Integer, ForeignKey("users.id"))
    question = relationship("Question", back_populates="answers")
    author = relationship("User", back_populates="answers")
    thread = relationship(
        "Thread", back_populates="original_answer", uselist=False, passive_deletes=True
    )
    reactions = relationship("Reaction", back_populates="answer", passive_deletes=True)


class Reaction(Base):
    __tablename__ = "reactions"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_onupdate=func.now())

    answer_id = Column(Integer, ForeignKey("answers.id", ondelete="CASCADE"))
    author_id = Column(Integer, ForeignKey("users.id"))
    answer = relationship("Answer", back_populates="reactions")
    author = relationship("User", back_populates="reactions")


class Thread(Base):
    __tablename__ = "threads"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_onupdate=func.now())

    original_answer_id = Column(Integer, ForeignKey("answers.id", ondelete="CASCADE"))
    original_answer = relationship("Answer", back_populates="thread")
    answers = relationship(
        "ThreadAnswer", back_populates="thread", passive_deletes=True
    )


class ThreadAnswer(Base):
    __tablename__ = "thread_answers"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_onupdate=func.now())

    text = Column(String)
    thread_id = Column(Integer, ForeignKey("threads.id", ondelete="CASCADE"))
    thread = relationship("Thread", back_populates="answers")
    author_id = Column(Integer, ForeignKey("users.id"))
    author = relationship("User", back_populates="thread_answers")
