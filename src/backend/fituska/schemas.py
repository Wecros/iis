from datetime import datetime
from typing import List, Optional

from fituska.models import UserType
from pydantic import BaseModel, create_model


class UserBase(BaseModel):
    username: str
    email: str
    name: str
    surname: str

    class Config:
        orm_mode = True
        use_enum_values = True


class UserUpdate(UserBase):
    role: UserType
    karma: int


class UserBaseWithID(UserBase):
    id: int


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


class ReactionBase(BaseModel):
    pass

    class Config:
        orm_mode = True


class ReactionCreate(ReactionBase):
    pass


class Reaction(ReactionBase):
    id: int
    answer_id: int
    author: UserBaseWithID


class ThreadAnswerBase(BaseModel):
    text: str

    class Config:
        orm_mode = True


class ThreadAnswerCreate(ThreadAnswerBase):
    pass


class ThreadAnswer(ThreadAnswerBase):
    id: int
    created_at: datetime
    updated_at: datetime = None
    author: UserBaseWithID
    thread_id: int


class ThreadBase(BaseModel):
    pass

    class Config:
        orm_mode = True


class ThreadCreate(ThreadBase):
    pass


class Thread(ThreadBase):
    id: int
    created_at: datetime
    updated_at: datetime = None
    original_answer_id: int
    answers: List[ThreadAnswer] = []


class AnswerBase(BaseModel):
    text: str

    class Config:
        orm_mode = True


class AnswerCreate(AnswerBase):
    pass


class Answer(AnswerBase):
    id: int
    created_at: datetime
    updated_at: datetime = None
    is_correct: bool
    question_id: int
    author: UserBaseWithID
    reactions: List[Reaction] = []
    thread: Thread = None


class QuestionBase(BaseModel):
    name: str
    details: str

    class Config:
        orm_mode = True


class QuestionCreate(QuestionBase):
    pass


class Question(QuestionBase):
    id: int
    created_at: datetime
    updated_at: datetime = None
    is_checked: bool
    category_id: int
    author: UserBaseWithID
    answers: List[Answer] = []


class CategoryBase(BaseModel):
    name: str

    class Config:
        orm_mode = True


class CategoryCreate(CategoryBase):
    pass


class Category(CategoryBase):
    id: int
    course_id: int
    questions: List[Question] = []


class CourseBase(BaseModel):
    name: str
    abbreviation: str
    description: str

    class Config:
        orm_mode = True


class CourseCreate(CourseBase):
    pass


class UserStudents(UserBase):
    id: int
    karma: int

    class Config:
        orm_mode = True


class CoursePublic(CourseBase):
    id: int
    teacher: UserBaseWithID
    categories: List[Category] = []


class CoursePublicForEndpoint(CoursePublic):
    registered_count: int
    question_count: int


class CoursePrivate(CourseBase):
    id: int
    teacher: UserBaseWithID
    categories: List[Category] = []
    students: List[UserStudents] = []
    approved: bool


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    is_active: bool
    karma: int
    role: UserType
    taught_courses: List[CoursePublic] = []
    registered_courses: List[CoursePublic] = []


class UserFame(BaseModel):
    username: str
    name: str
    surname: str
    karma: int

    class Config:
        orm_mode = True


class CourseFame(BaseModel):
    abbreviation: str
    id: int

    class Config:
        orm_mode = True


class IndividualCourseFame(BaseModel):
    course: CourseFame
    top_users: List[UserFame]


class HallOfFame(BaseModel):
    all_courses: List[UserFame]
    individual_courses: List[IndividualCourseFame]
