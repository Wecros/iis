from fastapi import Depends, HTTPException, status
from fituska import crud, models, schemas
from fituska.database import Base, SessionLocal, engine
from fituska.security import (
    ACCESS_TOKEN_EXPIRE_MINUTES,
    ALGORITHM,
    SECRET_KEY,
    oauth2_scheme,
    pwd_context,
)
from jose import JWTError, jwt
from passlib.context import CryptContext
from sqlalchemy.orm import Session


# Database dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def db_init():
    db = SessionLocal()

    # Uncomment following lines to delete all database data but keep the schema.
    # with contextlib.closing(engine.connect()) as con:
    # trans = con.begin()
    # for table in reversed(Base.metadata.sorted_tables):
    # con.execute(table.delete())
    # trans.commit()

    if crud.get_user_by_username(db, "admin"):
        return

    hashed_admin = pwd_context.hash("admin")
    hashed_moderator = pwd_context.hash("moderator")
    hashed_user1 = pwd_context.hash("uzivatel")
    hashed_user2 = pwd_context.hash("ucitel")
    hashed_user3 = pwd_context.hash("password")
    admin = models.User(
        username="admin",
        email="admin@stud.fit.vutbr.cz",
        name="Pavel",
        surname="Otépka",
        hashed_password=hashed_admin,
        role="admin",
    )
    moderator = models.User(
        username="moderator",
        email="moderator@stud.fit.vutbr.cz",
        name="Roman",
        surname="Petržela",
        hashed_password=hashed_moderator,
        role="moderator",
    )
    user1 = models.User(
        username="uzivatel",
        email="uzivatel@stud.fit.vutbr.cz",
        name="Petr",
        surname="Rychlý",
        hashed_password=hashed_user1,
        role="standard",
    )
    user2 = models.User(
        username="ucitel",
        email="ucitel@stud.fit.vutbr.cz",
        name="Ondřej",
        surname="Blátivý",
        hashed_password=hashed_user2,
        role="standard",
    )
    user3 = models.User(
        username="xfilip46",
        email="wecros@gmail.com",
        name="Marek",
        surname="Filip",
        hashed_password=hashed_user3,
        role="standard",
    )

    its = models.Course(
        name="Testování a dynamická analýza",
        abbreviation="ITS",
        description="Specifikace testů. Kritéria pokrytí zdrojových kódů (EC, NC, EPC, PPC). Kritéria pokrytí logických výrazů (PC, CC, MCDC). Rozklad vstupní domény. Kritéria pokrytí vstupní domény. Black-box analýza. Sledování práce se sdílenými prostředky. Testování vícevláknových aplikací. Testování síťových aplikací. Testování GUI. Výkonnostní testování. Řízení testování.",
        approved=True,
    )
    its.students.extend([user1, user3])
    its.teacher = user2

    ios = models.Course(
        name="Operační systémy",
        abbreviation="IOS",
        description="Pojem operačního systému (OS) jako součásti programového vybavení. Architektura OS, klasifikace OS. Přehled operačních systémů. Jádro OS UNIX, jeho struktura, textové a grafické uživatelské rozhraní, příkazové jazyky. Systémy ovládání souborů, základní principy implementace vstup/výstupních operací. Správa procesů, přepínání kontextu, plánování, meziprocesová komunikace.  Správa paměti, stránkování, virtuální paměť. Synchronizace procesů, semafory a další synchronizační prostředky, uváznutí, stárnutí.",
        approved=False,
    )
    ios.teacher = user2

    ios_project_1 = models.Category(
        name="Projekt 1",
        course_id=ios.id,
    )
    ios.categories.extend([ios_project_1])

    db.add_all([admin, moderator, user1, user2, user3])
    db.add_all([its, ios])

    db.commit()
    db.close()


def get_current_user(
    db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = schemas.TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = crud.get_user_by_username(db, username=token_data.username)
    if user is None:
        raise credentials_exception
    if not user.is_active:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Inactive user"
        )
    return user


def get_current_admin(current_user: models.User = Depends(get_current_user)):
    if not current_user.role == models.UserType.admin:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Insufficient user permissions",
        )
    return current_user


def get_current_moderator(current_user: models.User = Depends(get_current_user)):
    if (
        not current_user.role == models.UserType.moderator
        and not current_user.role == models.UserType.admin
    ):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Insufficient user permissions",
        )
    return current_user


def get_current_teacher(
    current_user: models.User = Depends(get_current_user), db: Session = Depends(get_db)
):
    ...


def get_current_teacher_or_moderator(
    current_user: models.User = Depends(get_current_user), db: Session = Depends(get_db)
):
    if (
        current_user.role == models.UserType.moderator
        or current_user.role == models.UserType.admin
    ):
        return current_user
