from fituska import models, schemas
from fituska.dependencies import (
    get_current_admin,
    get_current_moderator,
    get_current_user,
    get_db,
)
from fituska.security import (
    ACCESS_TOKEN_EXPIRE_MINUTES,
    ALGORITHM,
    SECRET_KEY,
    oauth2_scheme,
    pwd_context,
)
from jose import JWTError, jwt
from passlib.context import CryptContext
from sqlalchemy.orm import Session


def get_user(db: Session, user_id: int):
    return db.query(models.User).get({"id": user_id})


def get_user_by_username(db: Session, username: str):
    return db.query(models.User).filter(models.User.username == username).first()


def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()


def create_user(db: Session, user: schemas.UserCreate):
    hashed_password = pwd_context.hash(user.password)
    db_user = models.User(
        username=user.username,
        email=user.email,
        name=user.name,
        surname=user.surname,
        hashed_password=hashed_password,
        role="standard",
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def modify_user(db: Session, user: schemas.UserBase, user_id: int):
    modified = (
        db.query(models.User)
        .filter(models.User.id == user_id)
        .update(user.dict(), synchronize_session=False)
    )
    if modified > 0:
        db.commit()
    return modified


def delete_user(db: Session, user_id: int):
    updated_rows = (
        db.query(models.User)
        .filter(models.User.id == user_id)
        .update({"is_active": False})
    )
    db.commit()
    return updated_rows


def create_teacher_course(db: Session, course: schemas.CourseCreate, teacher_id: int):
    db_course = models.Course(**course.dict(), teacher_id=teacher_id)
    db.add(db_course)
    db.commit()
    db.refresh(db_course)
    return db_course
