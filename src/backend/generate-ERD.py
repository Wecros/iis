#!/usr/bin/env python3

import os

from dotenv import load_dotenv
from sqlalchemy import MetaData
from sqlalchemy_schemadisplay import create_schema_graph

load_dotenv()

POSTGRES_USER = os.getenv("POSTGRES_USER")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
POSTGRES_DB = os.getenv("POSTGRES_DB")

SQLALCHEMY_DATABASE_URL = (
    f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@localhost/{POSTGRES_DB}"
)

graph = create_schema_graph(metadata=MetaData(SQLALCHEMY_DATABASE_URL))
graph.write_png("fituska-erd.png")
